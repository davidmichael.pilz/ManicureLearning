#!pip install --upgrade google-api-python-client

################################################################################################
################################################################################################
################################### imports and globals ########################################
################################################################################################
################################################################################################

import numpy as np
import urllib.request
import googleapiclient.discovery
import googleapiclient.errors
import sys
from random import random
from os.path import exists

import json

################################################################################################
################################################################################################
######################################### functions ############################################
################################################################################################
################################################################################################

def save_thumbnail_from_videoid(video_id, filepath):
    '''
    IN:
        - videoid: id of youtube video
        - filepath: path where thumbnail image will be saved to
    OUT:
        - None
    INFOS:save_channel_thumbnails
        saves the thumbnail of a given youtube video to a local file, via http request
    '''

    thumbnail_url_prefix = "https://img.youtube.com/vi/"
    #thumbnail_url_suffix = "/default.jpg"
    #thumbnail_url_suffix = "/maxresdefault.jpg"
    thumbnail_url_suffix = "/sddefault.jpg"
    thumbnail_url = thumbnail_url_prefix + video_id + thumbnail_url_suffix

    urllib.request.urlretrieve(thumbnail_url, filepath)
    return


def save_meta_from_videoids(video_ids, apiclient, directory_path, file_name):
    ##
    #TODO:
    #   - DOCU!
    ##
    '''
    IN:
        - video_id <list>: list of video-id's of which we want the meta data
        - apiclient <googleapiclient>: created eg. by googleapiclient.discovery.build
        - directory_path <string>: path where to save
        - file_name <string>: name of meta_data file

    OUT:
        - <dictionary> of video-id as key and meta data as item
    INFOS:
        - loads and saves all the metadata and thumbnail-pngs of a given list of video ids
        - loads the existing content in the given filepath, if a file with the name already exists
        -> can load the videodata over several calls of the same function, does remove dublicates aswell
    '''
    bool = False
    with(open("datafiles/MrBeast/not_used_video_Ids.txt", "a+")) as p:
        if exists(directory_path+file_name):
            f = open(directory_path+file_name)
            meta_data = json.load(f)
            f.close()

            for key in meta_data:
                if key in video_ids:
                    video_ids.remove(key)
        else:
            meta_data = {}

        not_used_video_Ids = p.read()[:-1].split(" ")
        not_used_video_Ids.pop(0)
        if not_used_video_Ids:
            for video_Id in not_used_video_Ids:
                video_ids.remove(video_Id)

        for i, v_id in enumerate(video_ids):
            try:
                request = apiclient.videos().list(
                    part="statistics, snippet, status, contentDetails",
                    id=v_id,
                    maxResults=50
                )
                response = request.execute()
                # Save metadata into dictionary
                if not response["items"]:
                    print("Items doesn't have any data stored! - Skip video")
                    print(v_id, file = p, end = ' ')
                    bool = True
                else:
                    meta_data[v_id] = {**response["items"][0]["snippet"], **response["items"][0]["statistics"], **response["items"][0]["status"], **response["items"][0]["contentDetails"]}
            except googleapiclient.errors.HttpError as errormessage:
                if is_api_error_because_full_key(errormessage):
                    print("\n\nHere the last videoid, where error occured:\n", v_id)
                    break
                else:
                    print("unknown googleapiclient.errors.HttpError, break loop")
                    print("\n\nHere the last videoid, where error occured:\n", v_id)
                    break
            else:
                try:
                    #save thumbnail of videoid
                    save_thumbnail_from_videoid(v_id, directory_path+"thumbnail_"+v_id+".png")
                    print(f"Number of Videos: {i}", end='\r')
                except urllib.error.HTTPError:
                    if bool == True:
                        print("Http Error while loading thumbnail: desired quality probably doesn't exist! - Skip video")
                        print(v_id, file = p, end = ' ')
                        bool = False
                    else:
                        meta_data.pop(v_id)
                        print("Http Error while loading thumbnail: desired quality probably doesn't exist! - Skip video")
                        print(v_id, file = p, end = ' ')




        # write metadata dictionary to file
        with open(directory_path+file_name,"w") as f:
            json.dump(meta_data, f)
        return v_id


def get_channel_names(channel_ids, apiclient):
    # maybe unneccessary fct? mainly implemented it for testing - Theo
    #### list(        part="snippet",        id=','.join(channel_ids),        maxResults=50) -> not in the right order !!!!
    '''
    IN:
        - channel_ids <list>: channel ids of the youtube channels one is intrested in (max 50)
        - apiclient: youtube apiclient
    OUT:
        - <list> of the names of the given channels
    INFOS:
        returns list of names of the channels with the given ids
    '''
    '''
    if len(channel_ids) > 50:
        print("Error: max number of channels: 50")
        return 1
    '''

    channel_names = []

    for channel in channel_ids:
        request = apiclient.channels().list(
            part="snippet",
            id=channel
            #id=','.join(channel_ids),
            #maxResults=50
        ).execute()


        for item in request["items"]:
            channel_names.append(item["snippet"]["title"])

    return channel_names


def list_videoids_of_channel(channel_id, apiclient, filepath=None):
    # # TODO:
    # -> is the order of the videos time ordered? (i think yes, but check explicitly)
    '''
    IN:
        - channel_id: id (<str>) of the youtube channel
        - apiclient: <googleapiclient>, created eg. by googleapiclient.discovery.build
        - filepath: filepath (<str>) to print the videoids to, if None, then nothing is printed
    OUT:
        - <list> of video ids of the given channel
    INFOS:
        returns video ids of a youtube channel, may save to file, if path is specified
    '''

    # get the uploads playlist id of the given channel:
    request = apiclient.channels().list(
        part="contentDetails",
        id=channel_id
    )
    response = request.execute()
    uploads_playlist_id = response["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"]

    # get all video ids in the uploads playlist of the given channel:
    video_ids = []
    page_token = None
    while True:
        request = apiclient.playlistItems().list(
            part="contentDetails",
            playlistId=uploads_playlist_id,
            maxResults=50,
            pageToken=page_token
        )
        response = request.execute()

        for video_data in response["items"]:
            video_ids.append(video_data["contentDetails"]["videoId"])

        if "nextPageToken" not in response:
            break
        page_token = response["nextPageToken"]

    # save videoids to file, if path is given
    if filepath is not None:
        with open(filepath, "w") as f:
            print(' '.join(video_ids), file=f)

    return video_ids


def save_channel_thumbnails(channel_id, apiclient, thumbnails_dir, videoids_filepath=None):
    ###########
    #### NOT TESTED!!!
    ###########


    '''
    IN:
        - channel_id: id (<str>) of the youtube channel
        - apiclient: <googleapiclient>, created eg. by googleapiclient.discovery.build
        - thumbnails_dir: filepath (<str>) (ending with "/") to save the thumbnails to ; there the videoids are saved as well if videoids_filepath is None
        - videoids_filepath: filepath (<str>) to get the videoids from (videoids in one line seperated by ' ') ; if None, list is retrieved from api
    OUT:
        - None
    INFOS:
        saves thumbnails to thumbnails_dir
    '''

    # get all video ids in the uploads playlist of the given channel:
    if videoids_filepath is None:
        videoidslist = list_videoids_of_channel(channel_id, apiclient, thumbnails_dir + "videoids_" + channel_id + ".txt")
    else:
        with open(videoids_filepath, "r") as f:
            videoidslist = f.read()[:-1].split(" ")

    # save the corresponding thumbnails in the thumbnails_dir
    for video in videoidslist:
        save_thumbnail_from_videoid(video, thumbnails_dir + "thumbnail_" + video + ".png")

    return 0


def list_subscribedToChannels_of_channelid(channel_id, apiclient):

    ####### NOT TESTED
    # can throw http error, can make it so that it is caught here (syntax see clustering_youtube)

    '''
    IN:
        - channel_id: id (<str>) of the youtube channel
        - apiclient: <googleapiclient>, created eg. by googleapiclient.discovery.build
    OUT:
        - <list> of channel ids to which the chosen channel subscribed, None if this data is not public
    INFOS:
        returns list of channel ids to which the chosen channel subscribed
    '''
    page_token=None
    subscribed_to_channels_list = []

    while True:
        response = apiclient.subscriptions().list(
            channelId=channel_id,
            part='snippet',
            maxResults=50,
            pageToken=page_token
        ).execute()

        for item in response["items"]:
            subscribed_to_channels_list.append(item["snippet"]["resourceId"]["channelId"])

        if 'nextPageToken' not in response:
            break
        page_token = response["nextPageToken"]

    return subscribed_to_channels_list


def list_comments_of_videoid(video_id, apiclient, filepath=None, n_comments=None):

    ##
    # TODO:
    #   - item['snippet']['topLevelComment']['snippet']['authorChannelId']['value'] does not always return the ID !!!!!!!!!!!!!
    #
    #   - save all (relevant) comment content to file!
    #   - ordering of comments in list, how? -> newest first? -> ref
    #   - save comment metadata? only id saved for now
    ##

    '''
    IN:
        - video_id: id (<str>) of the youtube video
        - apiclient: <googleapiclient>, created eg. by googleapiclient.discovery.build
        - filepath: filepath (<str>) to print the comments to, if None, then nothing is printed
        - n_comments <int>: speciefies the number of comments to get, load the whole list, if None
    OUT:
        - <list> of comment channel ids of the given video, <list> of comment details of the given video
    INFOS:
        returns comments and channel ids of the commentors of a youtube video, may save to file, if path is specified
    '''

    comments_list = []
    comment_channel_ids_list = []
    page_token = None

    # get all comments of a given video
    while True:
        response = apiclient.commentThreads().list(
            videoId=video_id,
            part='snippet',
            textFormat='plainText',
            maxResults=100,
            pageToken=page_token
        ).execute()

        # retrieve comment / comment_channel_id from video
        for item in response['items']:
            try:
                ##### sometimes not channelID but something completely else??!?
                comment_channel_id = item['snippet']['topLevelComment']['snippet']['authorChannelId']['value']
                comment = item #item['snippet']['topLevelComment']['snippet']['textDisplay']
                comments_list.append(comment)
            except:
                ## probably "KeyError: 'authorChannelId'" occured ?? (no authorChannelId, empty authorChannelUrl)
                print("ERROR RAISED in list_comments_of_videoid")
                print(item)
                continue

            comment_channel_ids_list.append(comment_channel_id)

        if n_comments is not None:
            if n_comments < len(comment_channel_ids_list):
                break

        # get next page of comments while next page available
        if 'nextPageToken' not in response:
            break
        page_token = response["nextPageToken"]


    # save comment ids to file, if path is given
    if filepath is not None:
        with open(filepath, "w") as f:
            if n_comments is not None:
                print("#potentially not the full list of comments, max number is: " + str(n_comments), file=f)
            print(' '.join(comment_channel_ids_list), file=f)

    return comment_channel_ids_list, comments_list

def is_api_error_because_full_key(api_error):
    '''
    IN:
        - api_error <googleapiclient.errors.HttpError>: Http error to be checked
    OUT:
        - <bool> whether the error was because a key is full (True) or not (False)
    INFOS:
        check whether an exception error was because of a full key
    '''
    if api_error.resp.get('content-type', '').startswith('application/json'):
        reason = json.loads(api_error.content).get('error').get('errors')[0].get('reason')
    if reason == "quotaExceeded":
        print("Current API-Key does not have any quota left.")
        return True

    return False



def cluster_youtube(starting_channel_ids, apiclient, n_commentor_pings=10, n_video_pings=10, latest_videos=True, latest_comments=False, max_n_channels=10, len_comments_list=500, deepness=None, load_state=False, state_file="./datafiles/clustering_state.json"):

        #### NOT TESTED

        #TODO:
        # - if often occuring, that subscribed channels not visible: to save quota: check a list of channels togehter, if they are private or not
        # - ordered dictionary for occurence list? more efficient
        # - printing order of clustered channels at the end strange? -> why?? ---> error in get_channel_names !!

        # - QUOTA COSTS -> can safe state of function and return later!!!!!!!!!!



        ### Not done, stuff to do ( --> now done, but have to check everything)
        # maybe need to add a check if the subscriber list is private
        # youtube shorts might pose a problem
        # could outsource some fct
        # handle disabled comments and stuff !!!!! (stuff = disable subscribed to channels)

        #   - suggestion by Theo: go through staring channels, do x times:
        #     <go to random video (or newest, maybe better), get random comment, check if subscribed channels are
        #     visible, if yes: get list of subscribed channels>
        #     -> build an overall dictionary/list of the number of times each channel occurs this way
        #     -> add the channel with the highest occurence/several channels with highest occurence to this
        #     (could also go for liked videos->channels of those videos AND, if we want to get a subgroup of youtube,
        #     channels, which are subscribed by many in general might noise our results here and occur in the list, even though they don't fit thematically
        #     here a possible solution might be taking a subgroup of channels with overall low subscriber numbers and excluding ones with high subscribers
        #     or: just taking the subspace of youtube of channels with high subscriber numbers and starting at eg. mrbeast)

        # can be made more efficient, eg. in saving the occurences in an ordered list (ordered by id) or maybe even better, ordered by number of occurences??
        # could aswell not load the whole list of comments, but just one/a subset
    '''
    IN:
        - starting_channel_ids <list>: handpicked starting points (channel ids) to branch out from
        - apiclient:
        - n_commentor_pings <int>: number of pings to commentors channels in given video
        - n_video_pings <int>: number of pings to videos of given channel
        - latest_videos <bool>: True, if latest videos are to be used, otherwise random ones are
        - max_n_channels <int>: maximum number of channels to be returned by the function
        - len_comments_list <int>: max length of the commentslist of videos to get, if None, all comments are loaded
        - deepness <int>: limit of additional channels to loop through for building the subscriber occurence list, if not specified: every new channel is looped through, otherwise: 1 stands for load only the subscribers of people in the comments of the starting cluster -> ...
        - load_state <bool>: if True then the old state of the function is loaded from the state_file, if false,  a fresh run is started
        - state_file <str>: filepath for the savestate of the function to be saved at (in json format)
    OUT:
        - <list> of channels found (new channels + staring channels), or None if function stopped early
    INFOS:
        returns list of channels in the bubble of the given starting channels, measured by cross subscribers via commentors
        !-> directory stuff: make sure thats obvious and generalized
    '''
    if deepness is None:
        deepness = max_n_channels-len(starting_channel_ids)

    #set starting params from save_state_file or to their starting values depending on load_state
    if load_state:
        print("LOAD: Loading state of the last run.")
        f = open(state_file)
        function_variables = json.load(f)
        f.close()
    else:
        #one dict for all the function variables to make saving an loading the function state more convenient
        function_variables = {}
        function_variables["channel_ids_list"] = starting_channel_ids.copy()
        function_variables["channel_occurences"] = {}
        function_variables["root_channel_starting_i"] = 0
        function_variables["video_starting_i"] = 0
        function_variables["commentor_starting_i"] = 0
        function_variables["recovering_after_error_in_loading_video_list"] = False
        function_variables["recovering_after_error_in_loading_commentors_list"] = False
        function_variables["recovering_after_error_in_loading_subscribedToChannels_list"] = False

    # loop through all the channels
    # (beginning with the given starting channels, but also through the newly added ones)
    for root_channel_i, channel_id in enumerate(function_variables["channel_ids_list"]):
        # recover the savestate:
        if root_channel_i < function_variables["root_channel_starting_i"]:
            print("LOAD: Skipping root channel until last state.")
            if root_channel_i == (function_variables["root_channel_starting_i"]-1):
                function_variables["root_channel_starting_i"] = 0
            continue

        # recover the savestate:
        if not (function_variables["recovering_after_error_in_loading_commentors_list"] or function_variables["recovering_after_error_in_loading_subscribedToChannels_list"]):
            function_variables["recovering_error_in_loading_video_list"] = False
            #load list of videos of the channel: (or create the list, if the file does not exist yet)
            filepath_videoids = "./datafiles/video_Ids/videoIds_" + channel_id + ".txt"
            if exists(filepath_videoids):
                with open(filepath_videoids, "r") as f:
                    function_variables["video_ids"] = f.read()[:-1].split(" ")
            else:
                try:
                    function_variables["video_ids"] = list_videoids_of_channel(channel_id, apiclient, filepath_videoids)
                except googleapiclient.errors.HttpError as errormessage:
                    # dump function state to state_file, if api key contingent is empty
                    if is_api_error_because_full_key(errormessage):
                        function_variables["video_starting_i"] = 0
                        function_variables["commentor_starting_i"] = 0
                        function_variables["root_channel_starting_i"] = root_channel_i
                        function_variables["recovering_after_error_in_loading_video_list"] = True
                        print("Stopping while getting video_ids list.")
                        with open(state_file,"w") as f:
                            json.dump(function_variables, f)
                        return None
                    print("Unforseen error while loading the videos list of a channel. - continue programm, possably fatal")
        else:
            print("LOAD: Skipping loading video list, because of before stop at loading <commentors list> or <subscribed to channels list>.")


        #loop through subset of videos of the channel from which to grab the commentors
        for video_i in range(n_video_pings):
            # recover the savestate:
            if video_i < function_variables["video_starting_i"]:
                print("LOAD: Skipping video until last state.")
                if video_i == (function_variables["video_starting_i"]-1):
                    function_variables["video_starting_i"] = 0
                continue
            # recover the savestate:
            if not (function_variables["recovering_after_error_in_loading_subscribedToChannels_list"] or function_variables["recovering_after_error_in_loading_commentors_list"]):
                if len(function_variables["video_ids"]) == 0:
                    print("number of videos lower than n_video_pings or all videos private - continue with lower number of pings")
                    break

                # take a random video or the (_current_) first one, depending on latest_videos
                if latest_videos:
                    video_listindex = 0
                else:
                    video_listindex = int(random()*len(function_variables["video_ids"]))
                function_variables["video_id"] = function_variables["video_ids"][video_listindex]
                function_variables["video_ids"].pop(video_listindex)
            else:
                print("LOAD: Skipping getting new video_id, because of before stop at loading <commentors list> or <subscribed to channels list>.")

            # recover the savestate:
            if not function_variables["recovering_after_error_in_loading_subscribedToChannels_list"]:
                function_variables["recovering_after_error_in_loading_commentors_list"] = False
                #load list of commentors of the video: (or create the list, if file does not exist yet)
                filepath_commentorids = "./datafiles/comments/comments_" + function_variables["video_id"] + ".txt"
                if exists(filepath_commentorids):
                    with open(filepath_commentorids, "r") as f:
                        function_variables["commentor_ids"] = f.read()[:-1].split(" ")
                else:
                    try:
                        ###### does not always return _ONLY_ commentorIDs!!!!!
                        function_variables["commentor_ids"], _ = list_comments_of_videoid(function_variables["video_id"], apiclient, filepath=filepath_commentorids, n_comments=len_comments_list)
                    except googleapiclient.errors.HttpError as errormessage:
                        # dump function state to state_file, if api key contingent is empty
                        if is_api_error_because_full_key(errormessage):
                            function_variables["root_channel_starting_i"] = root_channel_i
                            function_variables["commentor_starting_i"] = 0
                            function_variables["video_starting_i"] = video_i
                            function_variables["recovering_after_error_in_loading_commentors_list"] = True
                            print("Stopping while getting commentor_ids list.")
                            with open(state_file,"w") as f:
                                json.dump(function_variables, f)
                            return None
                        #else probably comments disabled:
                        print("HttpError in cluster_youtube: load comments of a video - skip video")
                        video_i -= 1
                        continue
            else:
                print("LOAD: Skipping loading commentors list, because of before stop at loading <subscribed to channels list>.")

            if not (root_channel_i < len(starting_channel_ids)-1+deepness):
                continue
            #loop through subset of comments of the video to grab the channels to which the commentor subscribed
            for commentor_i in range(n_commentor_pings):
                # recover the savestate:
                if commentor_i < function_variables["commentor_starting_i"]:
                    print("LOAD: Skipping commentor until last state.")
                    if commentor_i == (function_variables["commentor_starting_i"]-1):
                        function_variables["commentor_starting_i"] = 0
                    continue
                # recover the savestate:
                if not function_variables["recovering_after_error_in_loading_subscribedToChannels_list"]:
                    #get a random commentor:
                    if len(function_variables["commentor_ids"]) == 0:
                        print("number of comments lower than n_commentor_pings - continue with lower number of pings")
                        break
                    # take a random comment or the first one, depending on latest_comment
                    if latest_comments:
                        commentor_listindex = 0
                    else:
                        commentor_listindex = int(random()*len(function_variables["commentor_ids"]))
                    function_variables["commentor_id"] = function_variables["commentor_ids"][commentor_listindex]
                    function_variables["commentor_ids"].pop(commentor_listindex)
                else:
                    print("LOAD: Skipping getting new commentor_id, because of before stop at loading <subscribed to channels list>.")

                function_variables["recovering_after_error_in_loading_subscribedToChannels_list"] = False
                #get the list of channel_ids to which the commentor subscribed
                try:
                    function_variables["subscribed_to_channels_list"] = list_subscribedToChannels_of_channelid(function_variables["commentor_id"], apiclient)
                except googleapiclient.errors.HttpError as errormessage:
                    # dump function state to state_file, if api key contingent is empty
                    if is_api_error_because_full_key(errormessage):
                        function_variables["root_channel_starting_i"] = root_channel_i
                        function_variables["video_starting_i"] = video_i
                        function_variables["commentor_starting_i"] = commentor_i
                        function_variables["recovering_after_error_in_loading_subscribedToChannels_list"] = True
                        print("Stopping while getting subscribed_to_channels_list list.")
                        with open(state_file,"w") as f:
                            json.dump(function_variables, f)
                        return None
                    # else probably subscriptions on private   --- ANNNNNNDD wrong commentor_id  !!!!??!
                    print("HttpError in cluster_youtube: load subscribed to channels of commentor - skip channel: " + function_variables["commentor_id"]) ##if debug
                    commentor_i -= 1
                    continue


                ### occurence is not really interpretable, as the 'old' stats are getting drag through the for loops ! ---> myabe we want to change that ?!
                #add those to the dictionary is the algorithm is not too "deep" yet
                for subscribed_to_channel_id in function_variables["subscribed_to_channels_list"]:
                    if subscribed_to_channel_id in function_variables["channel_occurences"]:
                        function_variables["channel_occurences"][subscribed_to_channel_id] += 1
                    else:
                        function_variables["channel_occurences"][subscribed_to_channel_id] = 1

        #stop the search if the desired number of channels is reached
        if len(function_variables["channel_ids_list"]) == max_n_channels:
            break

        # if all the starting channels were looped through, add new channel to the channels list
        if root_channel_i >= len(starting_channel_ids) - 1:
            #sort channels by their occurences (descending order)
            channel_occurences_ids = np.array(list(function_variables["channel_occurences"].keys()))
            channel_occurences_n_occurences = np.array(list(function_variables["channel_occurences"].values()))
            sort_order = np.argsort(channel_occurences_n_occurences)
            channel_occurences_ids = np.flip(channel_occurences_ids[sort_order])

            #add the channel with highest occurences, which is not already in the list of channel ids of the cluster
            for potential_additional_channel in channel_occurences_ids:
                if potential_additional_channel in function_variables["channel_ids_list"]:
                    continue
                else:
                    function_variables["channel_ids_list"].append(potential_additional_channel)
                    break

        print("Updated channels list (%d/%d): " %(len(function_variables["channel_ids_list"]), max_n_channels), function_variables["channel_ids_list"])

    return function_variables["channel_ids_list"]


def cluster_youtube_over_several_keys(starting_channel_ids, api_keys, n_commentor_pings=10, n_video_pings=10, latest_videos=True, latest_comments=False, max_n_channels=10, len_comments_list=500, deepness=None, load_state=False, state_file="./datafiles/clustering_state.json"):
    '''
    IN:
        - starting_channel_ids <list>: handpicked starting points (channel ids) to branch out from
        - api_keys <list>: youtube api keys <str>
        - n_commentor_pings <int>: number of pings to commentors channels in given video
        - n_video_pings <int>: number of pings to videos of given channel
        - latest_videos <bool>: True, if latest videos are to be used, otherwise random ones are
        - max_n_channels <int>: maximum number of channels to be returned by the function
        - len_comments_list <int>: max length of the commentslist of videos to get, if None, all comments are loaded
        - deepness <int>: limit of additional channels to loop through for building the subscriber occurence list, if not specified: every new channel is looped through, otherwise: 1 stands for load only the subscribers of people in the comments of the starting cluster -> ...
        - load_state <bool>: if True then the old state of the function is loaded from the state_file, if false,  a fresh run is started
        - state_file <str>: filepath for the savestate of the function to be saved at (in json format)
    OUT:
        - <list> of channels found (new channels + staring channels)
    INFOS:
        returns list of channels in the bubble of the given starting channels, measured by cross subscribers via commentors, using several api keys
    '''
    api_service_name = "youtube"
    api_version = "v3"

    for api_key_i, api_key in enumerate(api_keys):
        print(f"Starting clustering with key {api_key_i+1}.")
        youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=api_key)
        channel_ids_list = cluster_youtube(starting_channel_ids, youtube, n_commentor_pings=n_commentor_pings, n_video_pings=n_video_pings, latest_videos=latest_videos, latest_comments=latest_comments, max_n_channels=max_n_channels, len_comments_list=len_comments_list, deepness=deepness, load_state=load_state, state_file=state_file)
        load_state = True
        if channel_ids_list is not None:
            break

    if channel_ids_list is not None:
        print("Clustering finised.")
        print(channel_ids_list)
        print(get_channel_names(channel_ids_list, youtube))
    else:
        print("Clustering done as far as possible with given keys. Repeat with <load_state = True> with new quota contigent to finish clustering.")
    return channel_ids_list



################################################################################################
################################################################################################
###################################### main programm ###########################################
################################################################################################
################################################################################################

api_key1 = "AIzaSyAtiwdiN_m5GA2BQgYPdcu198HmYWNZfgQ"
api_key2 = "AIzaSyBqXRHe1qbNByq7507tJZ-_vZI9fvJU860"
api_key3 = "AIzaSyDu74nlzNAxtwkJcwUc9RbCbY46U-_IKHA"
api_key4 = "AIzaSyDF1DrESXP04wUrRJ78Wj4e-l0NO9k840Q"
api_key5 = "AIzaSyBAwCg5Xck9yRm2oJadBZokhZuig3zP22c"
api_key6 = "AIzaSyDGyXaQ6rTQe2a7vKGn6IO6YLLOBwhp_T0"

api_keys = [api_key1, api_key2, api_key3, api_key4, api_key5, api_key6]

starting_channels = ["UCX6OQ3DkcsbYNE6H8uQQuVA"]
#cluster_youtube_over_several_keys(starting_channels, api_keys, n_commentor_pings=10, n_video_pings=10, max_n_channels=1000, deepness=None, load_state=True)


#exit()
api_service_name = "youtube"
api_version = "v3"
api_key = api_key2
youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=api_key)
#'''
channels = ['UCX6OQ3DkcsbYNE6H8uQQuVA', 'UC-lHJZR3Gqxm24_Vd_AJ5Yw', 'UC7_YxT-KID8kRbqZo7MyscQ', 'UCo_IB5145EVNcf8hw1Kku7w', 'UC3sznuotAs2ohg_U__Jzj_Q', 'UCHYoe8kQ-7Gn9ASOlmI0k6Q', 'UCYzPXprvl5Y-Sf0g4vX-m6g', 'UCIPPMRA040LQr5QPyJEbmXA', 'UCY1kMZp36IQSyNx_9h4mpCg', 'UCUaT_39o1x6qWjz7K2pWcgw', 'UC4-79UOlP48-QNGgCko5p2g', 'UCFAiFyGs6oDiF1Nf-rRJpZA', 'UCTkXRDQl0luXxVQrRQvWS6w', 'UCo8bcnLyZH8tBIH9V1mLgqQ', 'UCGwu0nbY2wSkW8N-cghnLpA', 'UCS5Oz6CHmeoF7vSad0qqXfw', 'UC3xZYc4SZUGfRERIvDRGqDQ', 'UC5p_l5ZeB_wGjO_yDXwiqvw', 'UC70Dib4MvFfT1tU6MqeyHpQ', 'UCmSp4bDxS9R0jpeZEvkut2g', 'UCJZam2u1G0syq3kyqrCXrNw', 'UCnmGIkw-KdI0W5siakKPKog', 'UClQubH2NeMmGLTLgNdLBwXg', 'UCAiLfjNXkNv24uhpzUgPa6A', 'UCke6I9N4KfC968-yRcd5YRg', 'UCyps-v4WNjWDnYRKmZ4BUGw', 'UCwIWAbIeu0xI0ReKWOcw3eg', 'UCpB959t8iPrxQWj7G6n0ctQ', 'UCbp9MyKCTEww4CxEzc_Tp0Q', 'UCm-X6o81nRsXQTmqpyArkBQ', 'UC_hK9fOxyy_TM8FJGXIyG8Q', 'UCRijo3ddMTht_IHyNSNXpNQ', 'UC_hSjGOO67A6gIDUcyuv_8A', 'UC0DZmkupLYwc0yDsfocLh0A', 'UCC-RHF_77zQdKcA75hr5oTQ', 'UCKYb5XBe-5OSEgLijLSoDtw', 'UC1n_yB_W-FZCbwajZPcs8Jw', 'UCXg4rJUbDP1IP3TmZ9KDpJg', 'UC7TTtOQKMXTWWMtWQMIgVSA', 'UC56D-IHcUvLVFTX_8NpQMXg', 'UC9yadlAEiyOUNa3y4dnuYFg', 'UCjPYYvIdTqn9U52p9IxJ72Q', 'UCuwJoiGWRxPYStBp0l0WuZw', 'UC4rlAVgAK0SGk-yTfe48Qpw', 'UCzYfz8uibvnB7Yc1LjePi4g', 'UCA2tt9GSU2sl8rAqjlLR3mQ', 'UCQVwQQvz8O_0aeFDBVcdhpg', 'UC6VAIqNQBc7ggiqvsOHOBqw', 'UC6D1L2vxEAg_Vi0JSxMBDgA', 'UCyagEfIN1okQ-s996XAqCFQ', 'UCB_CCSAGP_YCuR36_w2bG1w', 'UCSf0s2ogUVYpJPuzW1zpAOg', 'UCw1SQ6QRRtfAhrN_cjkrOgA', 'UC-g_O9HWb5B0rVDG1hLd5Hw', 'UCVAYy7_zddIx6USjXscV9Vg', 'UCwVg9btOceLQuNCdoQk9CXg', 'UC9tYJpGCSkzDt4HkPC4BtkA', 'UC7u9o8BHiJyH2_cef_nC7tQ', 'UCiZ2z8F8ilr6SC7lOaxAQ4g', 'UChd1FPXykD4pust3ljzq6hQ', 'UCR36JvZUKyrI5ekQuBKOVcg', 'UCYzEMRKqrh01-tauv7MYyVQ', 'UCb8vrqP8Z7Oz9ZTYvUtjUHQ', 'UC6E2mP01ZLH_kbAyeazCNdg', 'UCdC0An4ZPNr_YiFiYoVbwaw', 'UCEdvpU2pFRCVqU6yIPyTpMQ', 'UC4-eGDvOe41__-RiE0E9L6A', 'UCZzvDDvaYti8Dd8bLEiSoyQ', 'UCyeVfsThIHM_mEZq7YXIQSQ', 'UCETZ7OiW7-wqwb-6hsz3C4w', 'UCe56RtxwepRSyZ-b--SedOw', 'UCMNmwqCtCSpftrbvR3KkHDA', 'UCKaCalz5N5ienIbfPzEbYuA', 'UCOaMOXfe8EWH1GJyhqhXrAA', 'UCaEFegr3Rcs430VZWEoNstw', 'UCAtWPQw_rUROGGjEPSFboIA', 'UCxsk7hqE_CwZWGEJEkGanbA', 'UC_UnVDztkvE1hw064XFTNSQ', 'UCdVcQc_gdaXhlFx3UvG8pjA', 'UC5Y1pPVNm-odhiubuRO1D6A', 'UCJswRv22oiUgmT1FuFeUekw', 'UC766XIyusOpE7syokucRykA', 'UCbKWv2x9t6u8yZoB3KcPtnw', 'UCJsUvAqDzczYv2UpFmu4PcA', 'UCGt7X90Au6BV8rf49BiM6Dg', 'UCWlapPvorWltCjxA2OD9ELw', 'UCilwZiBBfI9X6yiZRzWty8Q', 'UCS7b93ZwoL1xt2hR7a7l2mg', 'UCja7QUMRG9AD8X2F_vXFb9A', 'UCZSlfzadjnw7G419c_OJ9eg', 'UCOs9JQ5-_V6GM5ZnjKnx5Tw', 'UC2C6_mSbiLp3CplZcgLTHXg', 'UC2hm5rD_IrfYRMfq5YQudgA', 'UCEuN0IauvXNTn6KhP9AVJVw', 'UCToxKVrkEuAONR4rFIJ_DyQ', 'UCDit-dp02_yDsn-zDxm7W4A', 'UCLmgvkCAGUhRh1Xiv3hg5HQ', 'UC0Wju2yvRlfwqraLlz5152Q', 'UC1WbRuMMzbzUfBG65x1FmXQ', 'UCBR8-60-B28hp2BmDPdntcQ', 'UCNSTteII-5yh617nIdK1vNQ', 'UCNAz5Ut1Swwg6h6ysBtWFog', 'UC0QHWhjbe5fGJEPz3sVb6nw', 'UC0h07r_UgTD0Tc-Dn5XLX3g', 'UCkQO3QsgTpNTsOw6ujimT5Q', 'UCHdMK5Ef2El8KbD3L_WgANg', 'UCEaReYkPVfExkfXptk0bSPw', 'UC1sELGmy5jp5fQUugmuYlXQ', 'UCfLuMSIDmeWRYpuCQL0OJ6A', 'UCBGp_XPj3Oudk207UNw_tAg', 'UC0VNRvMijMy6DtM9UgX46Og', 'UCsEgeyBfOnGGBpjIvkDJbWg', 'UCP0_k4INXrwPS6HhIyYqsTg', 'UCiciOsypkXcqSFqSPd-NRVA', 'UCCE5NVlm0FhbunMMBT48WAw', 'UCMiJRAwDNSNzuYeN2uWa0pA', 'UCr8Xk8otTXckBw2rXEQXMuA', 'UCGa9YJJiR-Jec5y10GQxEPg', 'UCT0qC6qz3d85rcyFZXxir9A', 'UCRKPNXzPWJtJeoVBaFVuBCw', 'UCAgNyzHvQllkZw4J3WJQd_A', 'UCKGNVB2moP-zeAWPmw8HvAA', 'UCn3AViOfcFN4zIICMC5hvPg', 'UCM1VmDe6DKW-I5d3tgO7R-A', 'UCeBnbqt4VRhotq2TQjkIi2A', 'UCUw4K8hh4No3ks8wxfqj-uQ', 'UCLCdyRg3u5O-EVP5p46QxcQ', 'UC4wOd-OGys-RKbv9FYFGRvA', 'UCJdxdc4MqvZRD9oFYoCl7wQ', 'UC6IijBsVf2PlWo_uq6P9wQw', 'UCiYcA0gJzg855iSKMrX3oHg', 'UC3IjgjgWtpLTCrR0shGXKVg', 'UCzMjRlKVO9XIqH_crIFpi6w', 'UCW0wEGsLzmIgvQDOXgaz4vg', 'UCUk7VggtJdo9XYTy3Z5QVAw', 'UC_WyXJkQH4ZytSn3EufdHNA', 'UC495kynFX55dE0iorwV3RKQ', 'UC8qNcrcCOpYIsWWwkX9mDGQ', 'UC1pYFrBsRJnoSwQIQ2kbN6A', 'UC8LcA3grYZg0GNpxlXh8owg', 'UCXtPHN6YyM6JGFooRwRfB7Q', 'UCicx_4RfqfiM07AXLv09Hww', 'UCfw6CRDOg5xQsB-2QNC_KUA', 'UCcJ3qKKZNOQIOlZjhW_UM8w', 'UCL375YQzDOi9v5y9b36KGhg', 'UC_cvTMeip9po2hZdF3aBXrA', 'UCrfHEKgEVE4mBp1DYcRAn4Q', 'UCt_FevX1UdOAMKGvXIzwTLg', 'UCpGdL9Sn3Q5YWUH2DVUW1Ug', 'UCWsDFcIhY2DBi3GB5uykGXA', 'UCIwZv2JVmkzM9OEflxvLKNQ', 'UC35TiyFAjzQCyOrFt5ccbhw', 'UC3HY3mmn2hBenusN53K6jZA', 'UC2wKfjlioOCLP4xQMOWNcgg', 'UC1I7kEKJD1s_ALwQ4U4OnLw', 'UCWLu3q9FEmPPYKcUAxKZb2A', 'UCMyOj6fhvKFMjxUCp3b_3gA', 'UCT80YajolSXW8_ST9TFYS3Q', 'UC-XTuGOHIF7vWvbhJZ56wbw', 'UC-sjC5fKKsrpcZRJwlZJzZQ', 'UCzqHFULLurtbHiqgULnaRQg', 'UC6xSN0oY2I9U_9xTo2RjE7g', 'UCPpATKqmMV-CNRNWYaDUwiA', 'UC5enhS5tdXhHNbj8s_kdBUw', 'UC1n_PfsVqxllCcnMPlxBIjw', 'UCV7lBBz6G20d87WoKFPjsPw', 'UC9DWJ33CMvIseLlyx6hyRnA', 'UC9tXyGZiEft9J4ZiI8dHb3Q', 'UCYn6CZe5UGIyPe9WJb0pTrg', 'UCYVinkwSX7szARULgYpvhLw', 'UCzeB_0FNcPIyUSjL_TL5lEw', 'UCR9Gcq0CMm6YgTzsDxAxjOQ', 'UChFur_NwVSbUozOcF_F2kMg', 'UC2D-GCxeE69hCUmRwISXABw', 'UCquKkmifC6eDU-bbKxqjJgw', 'UCaZ2jG4qd1vY-Tkwiq9FBlw', 'UC26FEMwAAjj8jfNbuVdFMBQ', 'UC0lkCn7D--uMrtwAXRj_cdw', 'UCJMT_d7BHncS49NnLmlt2wQ', 'UCItXVkLkMv3RbJesDZW0uYA', 'UCi-5OZ2tYuwMLIcEyOsbdRA', 'UCcG6HCBufJHMGFXRM-V0wJw', 'UClgRkhTL3_hImCAmdLfDE4g', 'UC3xjWr5aCjxbjg1JYxmeyRQ', 'UC8TOSHpiM1hjNTNyHReLe5g', 'UCom1fzUtWF8A7zDbHYMNCfA', 'UC4uKK_YwAxTfad3x8cAJb6A', 'UCfcY_q2HTeSM640HDrgK-3A', 'UC_k6e2PPDUnxEdMJC9Q044g', 'UCoP4V9_s2t5Wt3dZbGDRqSw', 'UC3gNmTGu-TTbFPpfSs5kNkg', 'UC-LAKZaLMI3Rspav1lEDYFw', 'UCgFvT6pUq9HLOvKBYERzXSQ', 'UCzkOp0ueqFF0yrwpimkjC9Q', 'UCljz_M6NK7ASp5u0t2ImfiQ', 'UCWBWgCD4oAqT3hUeq40SCUw', 'UCK7mk2tt1FtBs5VqocHQtzg', 'UCWc8CackfCo4q46FpEWBcPg', 'UCq8DICunczvLuJJq414110A', 'UCCPsBYxLUykj5xbgOGTRzKQ', 'UCRVtlcqayOmyuLIDrT3ng1w', 'UCtvbvzHVrc3SUqQerG1cFGA', 'UC0zjODUrI0kcZT_2XwmWVGA', 'UCJ896dajAilqJp_zcENm31Q', 'UCaQk0pF6pxjOe2_gSHQbiMw', 'UC01ZqcYQ6DbfedKhTRponiA', 'UCA0SDFtM9jm4GZozvvy9COA', 'UCBes8dXRO0BPwrGZOySzkbg', 'UCwaNuezahYT3BOjfXsne2mg', 'UCPAk4rqVIwg1NCXh61VJxbg', 'UCFPk5G440gIZTycgm64syeg', 'UCnRPe5CuTVf83QYgKJHWmUg', 'UCV9XelVTb5Ul_-Vs-QVUzOQ', 'UCoZMST5rl4yeL6sFW7JZaDQ', 'UCpfXVjy3p5qfR_7EoKpQirQ', 'UC3XqNmaUbVSdOcE2GxAA77g', 'UC3E8z5P4FemGLQRXixxHL3g', 'UCkShDLspTnaaiamkpY_UDHg', 'UCmr_vYKbp98f6kdRoer0iVw', 'UCTyC8MgBZhYrompfwfo-iqw', 'UCHb6TlHa5XCMV7fUjtD47lQ', 'UC1aZZH3PaSdhYT1p5wtgLwQ', 'UCzY_sIOOHeCobumpqJrjdig', 'UCAC51Lw_eHFBfZwhk2Q4C3A', 'UC9MAhLcXqWZo2CKlFjLpDEA', 'UCJkgCaLb6fy7PJB4nVLQC_w', 'UCO_8hF-oulap_6lXy0oXLjg', 'UCbSuvZ6FYYg6Q964ZIi9SOw', 'UCQNDoTI2g7_daH6zL_9rvWg', 'UC5wEWgN1dOUR6SLMOO-CuEg', 'UCewMTclBJZPaNEfbf-qYMGA', 'UCfRt399Q6kNLxS4YC8Xf8dw', 'UC3Np7DUewV3LhtmR8BVDrCQ', 'UCaqi3SRvGTlPptx0ns90UOA', 'UCh7EqOZt7EvO2osuKbIlpGg', 'UCzEy7pi3B7TIS9cn_sdKK9A', 'UCqVCSWnuOSPm26orvNDTuWQ', 'UChYARX5Yj8CapM-F2Z2vPxA', 'UCSOfUqPoqpp5aWDDnAyv94g', 'UC3AfLsEdJL-WNFPsvur3xQw', 'UCcY6UhjQ3txXXxHoWEnV3Gw', 'UCR_J_SntqJh5eXw66d5hJxA', 'UCvInsdoSCTRGQNuXe7kMjhQ', 'UCHlq0fpOYpOyHbtUqSTigdg', 'UCDRbNGFusqlXX4a5vwi9ouQ', 'UCmRY4NSGK52lP_Lz11CjdYw', 'UCfw8x3VR-ElcaWW2Tg_jgSA', 'UClFN9LShD_Pv0wnSeUKbUZw', 'UC7pp8pL0dwUXcbcah6J576w', 'UC9t2RlHUshAARQNGe3tfk0A', 'UC2IbKgetx0qzxVKVZ8NlIfw', 'UC3P8f5TZu7yjvXcEcD7PHrw', 'UCfdNM3NAhaBOXCafH7krzrA', 'UCjgpFI5dU-D1-kh9H1muoxQ', 'UCAW-NpUFkMyCNrvRSSGIvDQ', 'UCChwR7eLKtbl1KyI7VFASzQ', 'UCFrVj6wBM42y3XuKCAj8n5A', 'UCz5Ms8847Lmak08A5AiGOEw', 'UC5f5IV0Bf79YLp_p9nfInRA', 'UC3vXfu6qNQVbjma1tqwv1IA', 'UCmFzvquJYoSDFhd9W0JhZKw', 'UCWvvmgNGT87ovWicgm8L7IQ', 'UCaTzBPf2rmS3ClFbnmOzkqw', 'UC-191PmPI7iJ068ine2KfIA', 'UCt3kwwPNZqA-d2KSgDxaQkQ', 'UC_GHCt-0IXbVnfDTB6Y88hA', 'UChFs0d8syPIK6x8q4C6BrJw', 'UCI_hjK53AIDlpB-Rpr1WnwQ', 'UC8-qVif2ALGPs3C1yem7XLg', 'UC2z8HLrUbudYX_fTyLzpptw', 'UCMnQ3Ss4TBPDX-lH85Z0k7w', 'UCxd6ifFZycyUB45d2t9VPQw', 'UCoaI5is-bILT3XVctKl5hpA', 'UCMg6kBf5SkG4tG2I-59Ugiw', 'UCnSWkrRWNQWNhDusoWr_HXQ', 'UCrkfdiZ4pF3f5waQaJtjXew', 'UCelMeixAOTs2OQAAi9wU8-g', 'UC4qGmRZ7aLOLfVsSdj5Se2A', 'UCtU4rGKbLtFWUFmr45zgUTw', 'UCe8D5lo_zrqS8xtsNspeCgg', 'UC_oMKGSjJp838-WKqp0phiQ', 'UCAs2g738VsemcwZ8sRsiJ2g', 'UCBhNf3uDB4pFuww29O_wAJQ', 'UCyQHab6hU_5cqhK6wRS173w', 'UCAatjI5Yc8o7WZcHDbNw7eA', 'UCA0mlN90EHCizvo101nbr-g', 'UCwD4x63A9KC7Si2RuSfg-SA', 'UCstrYVM0-fSq3i2JJvaubBg', 'UCGZy8uySf4UUlVt4qrV2V5g', 'UC7l8Wy10AB7VqkjRqjLp2iA', 'UCePxRBreVaMy_ontn4Vz2Yg', 'UCjvGPQTRr2eIEhthL5y31VQ', 'UC2iUwfYi_1FCGGqhOUNx-iA', 'UC9k-yiEpRHMNVOnOi_aQK8w', 'UCX2mh673YYeJQgrXXxJGuYQ', 'UCsDlWZWCWnGS1k-dnoDXrOA', 'UCj-Hs9XuzY15HE1q5R_GnJQ', 'UCz5osAvTYFKjb6Kjcep6vXA', 'UCB4NFn-8oipHct0IfAQBQrQ', 'UC6nSFpj9HTCZ5t-N3Rm3-HA', 'UC4PooiX37Pld1T8J5SYT-SQ', 'UCfR-4yZACIMRs3wNf0hel6g', 'UCR4s1DE9J4DHzZYXMltSMAg', 'UC0v-tlzsn0QZwJnkiaUSJVQ', 'UCeoNfc08yy2cANszMa4c-AA', 'UC295-Dw_tDNtZXFeAPAW6Aw', 'UCG0_tcRGyQnGOJvZIuzHDnQ', 'UCZJsZO3KkeU9mMPWrz6dlVQ', 'UChfTcl5XfdTUCjkfuro982Q', 'UCPuEAY09CtdTzFNWuqVZgDw', 'UCIDQSqdVuNwtfRMKqfX-o7g', 'UC1HL3TABt6QGd_P857BH_CA', 'UCM9r1xn6s30OnlJWb-jc3Sw', 'UCIEv3lZ_tNXHzL3ox-_uUGQ', 'UCFRCiFkWWJN2rO6dsKU9-1A', 'UCA19mAJURyYHbJzhfpqhpCA', 'UC5Z7Fc_QmKPzHv1Zl0Yw6AA', 'UCEMOZ_fY37Eeu7vIAynxKUg', 'UCNSzfesc7IgWZwg4n6uXr1A', 'UCRyS4kYdsaGcuFczzCjFXag', 'UCR9NuNwCUIhMOLUQnLN7_WA', 'UCOpNcN46UbXVtpKMrmU4Abg', 'UCKQ-wNdh0kO5qnpPfXa2hjQ', 'UCBwSufNse8VMBvQM_rCSvgQ', 'UCqsBym4OHrzSp0Nq1eZoMIA', 'UCIKEnAGzEfvGSE6oqOmaFGA', 'UChNMDQstHsVQGMlBAWDyBEg', 'UC1gUkqb3oV__jVRY53lpZrg', 'UCJl0idSupgyRNT9uKfS9bxA', 'UC02rAIf5RYrww6AWhoI5S0w', 'UChKQYI9z5rO_sdNjhwyyjSg', 'UCk2s_j0cbkWy7bXRV3NLfZQ', 'UCBINYCmwE29fBXCpUI8DgTA', 'UC0C-w0YjGpqDXGB8IHb662A', 'UCdX5KXiCTPYWYZscfphgQ4g', 'UCWiXzvPDb5YqG7W0mr2TmlA', 'UC_R0mVVGfifzA-59nwZWE0w', 'UCSdnFL7FG3Kn8M5ZslUDRMw', 'UCI2hA8WukW-Q90aabtm3Hbw', 'UCZpKiKzlLvqKKkFQPtkiweA', 'UCUFJO5h2NuvCykRJUIkd7oA', 'UC-zD0q7ryv_OWPB_xEwJJIQ', 'UCjXFBw5ysBps77hpjGd81dQ', 'UCIIBYx9uXyAR-md-vOdBrDw', 'UCKIL4dMp2qbWElEEZHK-aug', 'UCeAQVFBUKGHpA2I2gfxuonw', 'UC-kbQRi1a4nybvCYoiCfZ7w', 'UCjiPEaapiHbJMoAdi_L8fNA', 'UCT4iTe4eB9aRn2p3jRuGlDA', 'UCoc2P3KddDp04bSa7cRES6Q', 'UC0AE_22J0kAo30yjasaeFqw', 'UCj4zC1Hfj-uc90FUXzRamNw', 'UCsXVk37bltHxD1rDPwtNM8Q', 'UCSS6UvWXW8OZD9wpIVkD0YA', 'UCOYWgypDktXdb-HfZnSMK6A', 'UCZf2Anm20qVqx7cU3ZKshdQ', 'UCcES0AbBihbgEXD51mIWF3Q', 'UCEqv6H9qZAD-zunGN47v0ug', 'UCNqFDjYTexJDET3rPDrmJKg', 'UC5X0rnyJL0PWd650l2T8_ng', 'UCCWp4CCmI2JmIaoAuv0ocEA', 'UCGIY_O-8vW4rfX98KlMkvRg', 'UCaWd5_7JhbQBe4dknZhsHJg', 'UCINb0wqPz-A0dV9nARjJlOQ', 'UC1zZE_kJ8rQHgLTVfobLi_g', 'UC42vK81HwLY-udUEJYz7efg', 'UCIabPXjvT5BVTxRDPCBBOOQ', 'UC9FkeEFIGd9FXRfxpTltXYA', 'UC6Mjg5R5QOJYjrio1JmP8Fg', 'UCq6VFHwMzcMXbuKyG7SQYIg', 'UCfM3zsQsOnfWNUppiycmBuw', 'UC7tHXQXWImq_0I3PCQ8Udaw', 'UC00M4ztrpj2rMwpG6nMlM8w', 'UCYHnYO50VV8SzsmQq5PChRA', 'UCt6XC9hvuZtlxfDYvI1WAEg', 'UCet_y01v87pE7MPGikjTQQw', 'UCO7XVLiv_5iVIaxAsnEIiLA', 'UCRlEFn0L2G_DktbyvN0AZ5A', 'UCiSVf-UpLC9rRjAT1qRTW0g', 'UCIVSqoHCUN1XdEpiVItxfoQ', 'UC24-XPATP4CeviFJ8SLiF8A', 'UC8-Th83bH_thdKZDJCrn88g', 'UC4QobU6STFB0P71PMvOGN5A', 'UCWwqHwqLSrdWMgp5DZG5Dzg', 'UCCOrp7GPgZA8EGrbOcIAsyQ', 'UC9SVhrnsGxWwiBRWT9c7Ozw', 'UC_uMv3bNXwapHl8Dzf2p01Q', 'UC91V6D3nkhP89wUb9f_h17g', 'UC8RBLJdSpxSVWDc_bGKmdQQ', 'UCBMvWaA97BAegvJ_Hz6Vwuw', 'UC6grEZ3znkiaglUaoVJoKiQ', 'UC8YgIoxMeCeMW_4U8C61VZw', 'UCBhKsmSx-fXcJpQtGFwoqDg', 'UCY30JRSgfhYXA6i6xX1erWg', 'UCESqho-TmmnJQJf4IWEbBkQ', 'UC2bW_AY9BlbYLGJSXAbjS4Q', 'UC3iwxKMRg5l2aDG_8XK2nSQ', 'UCzJ53fi4NuDCYWOk9PtEkCA', 'UCHOgE8XeaCjlgvH0t01fVZg', 'UCXuqSBlHAE6Xw-yeJA0Tunw', 'UCHnyfMqiRRG1u-2MsSQLbXA', 'UC1VLQPn9cYSqx8plbk9RxxQ', 'UC0rDDvHM7u_7aWgAojSXl1Q', 'UCKy1dAqELo0zrOtPkf0eTMw', 'UC_aEa8K-EOJ3D6gOs7HcyNg', 'UCHCph-_jLba_9atyCZJPLQQ', 'UC9CoOnJkIBMdeijd9qYoT_g', 'UCGmnsW623G1r-Chmo5RB4Yw', 'UCANBW6ACWi9huiCfbKk1pbQ', 'UCZXVJgry4FxvZO2718qQh5Q', 'UCDfk8ogO6QGeJAYCY0QDzKw', 'UCJ5v_MCY6GNUBTO8-D3XoAg', 'UClG8odDC8TS6Zpqk9CGVQiQ', 'UCZ3AmknSJtbzXCeO5a4peoQ', 'UCzwFZOhCfCZTgLruMz_RWCw', 'UChM5Ff2yVeIZ16EWiB081cg', 'UChLN0bJgq6d15OK2HVB4YTg', 'UCFUsBdbrNe2a8tnVsxBwoZw', 'UC-2Y8dQb0S6DtpxNgAKoJKA', 'UCKqH_9mk1waLgBiL2vT5b9g', 'UCt-GOpCw4dOBlIyqL9A1ztA', 'UC_8NknAFiyhOUaZqHR3lq3Q', 'UCKr0RAl4snaKO0vspEJSvoA', 'UCd1fLoVFooPeWqCEYVUJZqg', 'UCG1acfc735_BPZMEZjuW1Jg', 'UC_q5WZtFp36adwqhKpZzxwQ', 'UC2nZMhZ2qG5-xpqb440WLYg', 'UCMYevdNaWmPbuIB0PdPc9ZQ', 'UCqynl7rdtktKMQESdSBmE-g', 'UCimb_JTekgvUdH72_-ygv9A', 'UCiILZEIfk6V0MVCFP5zDK5A', 'UCT9zcQNlyht7fRlcjmflRSA', 'UCSpfz1IyUA1NBH-cgj8ygUw', 'UCrYrcFGGs_nke1MggS8Jvqg', 'UC1rsLSbsz0Y2blrWDZ_-8Qw', 'UCm99c6g9MKBL4lNTlWrjyFQ', 'UCFKwASpt-58XgabursEsHXQ', 'UCXHGoWpySS0smjanTUDa6Gw', 'UCAc-yX63ImMA6P5yCcgi1qA', 'UCtNBFUsxj2YVbEc5HquzFqg', 'UCss_0YiNRBiQzFLt2dosZIg', 'UCI4fHQkguBNW3SwTqmehzjw', 'UCz_yk8mDSAnxJq0ar66L4sw', 'UCTGOuh9b_Su2k5xEARHCQQA', 'UCY2ekMrWhsUVHwO3J3-PCzQ', 'UCMhD97ZasArNEhc7oOsE75A', 'UChU3JRloULzdFX3aCu7BiSA', 'UCF0_hwTeQ73IhJuEtsUbEtA', 'UC-O9o5coq8iFSVthivq__tw', 'UCD-WWj0WqSGQOynNigc5VHw', 'UCemhZ2At2lgifTJLClGCz9A', 'UCgwv23FVv3lqh567yagXfNg', 'UCXcZymr3NtvAW7O21BlDjSg', 'UCbcS5Nx2sRbORA8B5tMp21g', 'UCRyIP2oznv4UPAl1jmKS5kA', 'UCC6ulNJ-KDoIa3VI2gcvQpA', 'UCZpRTwgtk1H0cUAWrHZ1gzg', 'UCKmmERguliWTynG9OIoDhDw', 'UC2IYWWifoIht9T47z1Z5lJw', 'UC5OrDvL9DscpcAstz7JnQGA', 'UCBJycsmduvYEL83R_U4JriQ', 'UCG8rbF3g2AMX70yOd8vqIZg', 'UCrPUg54jUy1T_wII9jgdRbg', 'UC9HQXWbt14wTdr_Tzc8ZE0A', 'UCCsj3Uk-cuVQejdoX-Pc_Lg', 'UC06E4Y_-ybJgBUMtXx8uNNw', 'UCUK0HBIBWgM2c4vsPhkYY4w', 'UCFhXFikryT4aFcLkLw2LBLA', 'UCBvc7pmUp9wiZIFOXEp1sCg', 'UCWXCrItCF6ZgXrdozUS-Idw', 'UCl16xaNY2arX3OzZBvTlsPQ', 'UCPD_bxCRGpmmeQcbe2kpPaA', 'UCCigEaEk-_gOrNlPdseG1pA', 'UC6107grRI4m0o2-emgoDnAA', 'UCPJHQ5_DLtxZ1gzBvZE99_g', 'UCFNTq9XKHDNy_1-2lL0kqCg', 'UCFYMDSW-BzeYZKFSxROw3Rg', 'UCN64HIrZNqFQYZ2BuyY-4zg', 'UC8JfkMtNAp44vmzdtnL4wow', 'UCCn62cYVpl0e_GN-yo1H9yQ', 'UCMu5gPmKp5av0QCAajKTMhw', 'UCSpFnDQr88xCZ80N-X7t0nQ', 'UCBa659QWEk1AI4Tg--mrJ2A', 'UCPDXXXJj9nax0fr0Wfc048g', 'UCYUQQgogVeQY8cMQamhHJcg', 'UCJHA_jMfCvEnv-3kRjTCQXw', 'UCNIuvl7V8zACPpTmmNIqP2A', 'UCSAUGyc_xA8uYzaIVG6MESQ', 'UCEmig2PwKGUmaQ9xzFWJ_xA', 'UCto7D1L-MiRoOziCXK9uT5Q', 'UCiGm_E4ZwYSHV3bcW1pnSeQ', 'UC9gVCa6ap-KDayMRPg5wjIg', 'UCAaegDUlb7doIKo0Rc1F08g', 'UCHu2KNu6TtJ0p4hpSW7Yv7Q', 'UCHEf6T_gVq4tlW5i91ESiWg', 'UC9VX0KXNH20x9MCH3xGjisg', 'UCIwFjwMjI0y7PDBVEO9-bkQ', 'UCWOA1ZGywLbqmigxE4Qlvuw', 'UCI1XS_GkLGDOgf8YLaaXNRA', 'UCClNRixXlagwAd--5MwJKCw', 'UCu6v4AdYxVH5fhfq9mi5llA', 'UC6MFZAOHXlKK1FI7V0XQVeA', 'UCdJdEguB1F1CiYe7OEi3SBg', 'UCLFXk9J3O-hhOk0msOjKYdQ', 'UCHdos0HAIEhIMqUc9L3vh1w', 'UCB9_VH_CNbbH4GfKu8qh63w', 'UCzfyYtgvkx5mLy8nlLlayYg', 'UCtPrkXdtCM5DACLufB9jbsA', 'UCojEXrCBzO-cP2N5YlRcrWw', 'UCzEnk4KWFlSj_PjXLz0-GMA', 'UCeRjIpQ9koLZxfRnPa1z6Sg', 'UCONd1SNf3_QqjzjCVsURNuA', 'UC9mvRrl9o7rG65ABsGVvDBw', 'UCC552Sd-3nyi_tk2BudLUzA', 'UC6OwWfpp8qHf1rfnOXKfxBw', 'UCVtFOytbRpEvzLjvqGG5gxQ', 'UCDogdKl7t7NHzQ95aEwkdMw', 'UCSlyP9TyhaqjucGL5sdpQGg', 'UCJFp8uSYCjXOMnkUyb3CQ3Q', 'UCp68_FLety0O-n9QU6phsgw', 'UCq6aw03lNILzV96UvEAASfQ', 'UCP5tjEmvPItGyLhmjdwP7Ww', 'UC2C_jShtL725hvbm1arSV9w', 'UCvUmwreRrbxeR1mbmojj8fg', 'UCHpGOTBGfltnrLqxfJn6zog', 'UCNpcQ002WElJqT9r--R-Xfg', 'UCESiRVMacmdsijiOtKvOeKQ', 'UCgAyQyy6x3L-tmM1GICelGA', 'UC6iJijyc_13WMPtew7euhqQ', 'UCY8SLLJjWpS4sx1dEqECaIw', 'UC1pexh6EOqNmQPWFf6_zYSQ', 'UCUoplm_wctr69IxiXNIKQog', 'UCw1KJiZ0M85ehyEKcbjN_nQ', 'UCJrOtniJ0-NWz37R30urifQ', 'UC0qyk0UvaN1z80BQdVQM85g', 'UCTWv3oYrqh_DNAtr8Hm_zGw', 'UCd534c_ehOvrLVL2v7Nl61w', 'UCNvzD7Z-g64bPXxGzaQaa4g', 'UC67f2Qf7FYhtoUIF4Sf29cA', 'UC_jQ64mgxDbvATLv94lMwaw', 'UCPLMPHT-d8GZOqL_AHJFdQQ', 'UCAz5JW1_oryewk0eR-eP7Bw']
#print(get_channel_names(channels, youtube))
#'''
#exit()
'''----------------------------------- Für PEWDIEPIE ZEUGS ---------------------------------'''
'''
PewDiePie_channelId = 'UC-lHJZR3Gqxm24_Vd_AJ5Yw'
list_videoids_of_channel(PewDiePie_channelId, youtube, filepath='datafiles/PewDiePie/list_of_videoid.txt')
with open('datafiles/PewDiePie/list_of_videoid.txt', "r") as f:
    PewDiePie_videoids = f.read()[:-1].split(" ")
save_meta_from_videoids(PewDiePie_videoids, youtube, 'datafiles/PewDiePie/', 'PewDiePie_meta.txt')

'''
with(open("datafiles/MrBeast/videoIds.txt", "r")) as g:
    MrBeast_clustering_video_Ids = g.read()[:-1].split(" ")

save_meta_from_videoids(MrBeast_clustering_video_Ids, youtube, 'datafiles/MrBeast/', 'MrBeast_clustering_meta.txt')

















'''
## for n_commentor_pings=10, n_video_pings=10 possible to cluster around 43 channels per apikey
api_key = api_key1

youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=api_key)



#example: get all videos of a youtube channel (here BibisBeautyPalace):--------------------------
channel_id = "UCHfdTAyg5t4mb1G-3rJ6QsQ"


#list_videoids_of_channel(channel_id, youtube, "./datafiles/video_Ids/videoIds_BibisBeautyPalace_UCHfdTAyg5t4mb1G-3rJ6QsQ.txt")
#------------------------------------------------------------------------------------------------
channel_ids  = ['UCX6OQ3DkcsbYNE6H8uQQuVA', 'UC4-79UOlP48-QNGgCko5p2g', 'UCIPPMRA040LQr5QPyJEbmXA', 'UCUaT_39o1x6qWjz7K2pWcgw', 'UCAiLfjNXkNv24uhpzUgPa6A', 'UCFAiFyGs6oDiF1Nf-rRJpZA', 'UCTkXRDQl0luXxVQrRQvWS6w', 'UC-lHJZR3Gqxm24_Vd_AJ5Yw', 'UCY1kMZp36IQSyNx_9h4mpCg']

"""
video_ids = []
for channel_id in channel_ids:
    with open("datafiles/video_Ids/videoIds_"+channel_id+".txt", "r") as f:
        video_ids.extend(f.read()[:-1].split(" "))
save_meta_from_videoids(video_ids[:10], youtube, "./datafiles/videodata/", "metadata.txt")
"""


#testing the youtube clustering:

# handpicked starting points to branch out from
starting_channels = ["UCXup1ITuwbY5r_Gn2K2_TXA, UCLCb_YDL9XfSYsWpS5xr05Q, UCvU1c8D5n1Rue3NFRu0pJSw, UCG0E3naN7D-bgldOJvGHbtg, UCGWSzxsb4UXsQf_FRQ7rR1g, UCNpP_PH0Uj8Ldc7INQ-ih_Q, UCTXeJ33DzXI2veQ"]
# ViceGripGarage, ThunderHead289, DylanMcCool, JunkyardDigs, Cleetus McFarland
starting_channels = ["UCsfu-jdkX2_v2t3_igVQebg", "UC7ssYyYxo88laKkU0tW9j9A", "UC02Z6oIaU0LS3H_7YxUUdSA","UCS87-wYrvmexsLkBj0jhrnQ","UCh8f8vssLddD2PbnU3Ag_Bw"]
starting_channels = ["UCX6OQ3DkcsbYNE6H8uQQuVA"]

#cluster = cluster_youtube(starting_channels, youtube, n_commentor_pings=10, n_video_pings=10, max_n_channels=50, deepness=None)
#print(cluster)
#print(get_channel_names(cluster, youtube))
'''


'''
request = youtube.channels().list(
    part="snippet,contentDetails,statistics",
    id="BibisBeautyPalace"
)
response = request.execute()

print(response)
'''

'''
<<<<<<< HEAD
save_thumbnail_from_videoid("1afUnHEUCGg", "pictures/"+sys.argv[1])
=======
save_thumbnail_from_videoid("1afUnHEUCGg", "test.jpg")


<<<<<<< HEAD
with open(filepath, 'r') as f:
    data = json.load(f)
<<<<<<< HEAD
>>>>>>> 64df23a14f85283d5de027d9c7af5ddd655f9dde
=======
>>>>>>> 64df23a14f85283d5de027d9c7af5ddd655f9dde
=======
data = json.load(f)
>>>>>>> d958410ab9f57adcffdfa78d942cd653f0bf7020
'''
