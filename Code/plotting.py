################################################################################################
################################################################################################
################################### imports and globals ########################################
################################################################################################
################################################################################################

import urllib.request
import googleapiclient.discovery
import googleapiclient.errors

import matplotlib.pyplot as plt
from matplotlib.colors import hsv_to_rgb
import numpy as np
from datetime import datetime
from wordcloud import WordCloud

import json
import plotly.graph_objects as go

def calculate_PT(c):
    sec = 0
    char_pos = [(pos,char) for pos, char in enumerate(c) if not char.isdigit()]
    char_pos.pop(0)
    char_pos.pop(0)
    for i in range(len(char_pos)):
        if i == 0:
            sec += int(c[2:char_pos[0][0]])*letter_to_timefactor(char_pos[0][1])
        else:
            sec += int(c[(char_pos[i-1][0]+1):char_pos[i][0]])*letter_to_timefactor(char_pos[i][1])
    return sec

def letter_to_timefactor(c):
    if c == "H":
        return 60*60
    if c == "M":
        return 60
    if c == "S":
        return 1

class meta_with_clicks:
    def __init__(self, thumbnail_Ids_path, path_processed_data):
        self.ID = thumbnail_Ids_path
        self.path = path_processed_data

    def likes_against_what(self, what):
        saving_dict = {}
        counter = 0

        # Open the file where the thumbnails are saved
        with(open(self.ID)) as f:
            # go threw all of the thumbnails
            for lines in f:
                # load the meta_data of the thumbnail
                g = open(self.path+"thumbnail_"+lines[:-1]+".csv")
                meta_data = json.load(g)
                g.close()
                print(counter, end='\r')
                counter += 1
                #if counter == 5000:
                #    break

                # choose based on variable what
                if what == "color": # If you want to look at colors

                    # List of things I don't need:
                    no_need = ["clicks","title","publishedAt","duration","channelId","emotion","text"]

                    # iterate threw all the colors
                    for keys in meta_data.keys():
                        if keys not in no_need:
                            if keys in saving_dict.keys():
                                saving_dict[keys].append([int(meta_data["clicks"]),meta_data[keys]])
                            else:
                                saving_dict[keys] = [[int(meta_data["clicks"]), meta_data[keys]]]
                        else:
                            continue

                elif what == "emotion": # If you want to look at emotions
                    types = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']

                    # iterate threw all the faces captured
                    for face in meta_data['emotion']:
                        # iterate threw the percentages of the above mentioned types
                        for i, percentage_type_of_emotion in enumerate(face[3:]):
                            if types[i] in saving_dict.keys():
                                # append tuple of percentage to that emotion with clicks of thumbnail
                                saving_dict[types[i]].append((meta_data["clicks"], percentage_type_of_emotion))
                            else:
                                saving_dict[types[i]] = [(meta_data["clicks"], percentage_type_of_emotion)]

                elif what == "face_count": # If you want to look at the amount of faces

                    # iterate threw all the faces captured
                    if len(meta_data['emotion']) in saving_dict.keys():
                        # append clicks
                        saving_dict[len(meta_data['emotion'])].append(int(meta_data["clicks"]))
                    else:
                        saving_dict[len(meta_data['emotion'])] = [int(meta_data["clicks"])]

                elif what == "face_size": # If you want to look at face_size
                    #iterate threw all the faces captured
                    for face in meta_data['emotion']:

                        # make one big key
                        if "all" in saving_dict.keys():
                            # save size and clicks together
                            saving_dict['all'].append((face[2],meta_data["clicks"]))
                        else:
                            saving_dict['all'] = [(face[2],meta_data["clicks"])]

                elif what == "duration": # If you want to look at the length of videos
                    seconds = calculate_PT(meta_data["duration"])
                    if "all" in saving_dict.keys():
                        # save time and clicks together
                        saving_dict['all'].append((seconds,meta_data["clicks"]))
                    else:
                        saving_dict['all'] = [(seconds,meta_data["clicks"])]

                elif what == "date":
                    dt = datetime.strptime(meta_data['publishedAt'], '%Y-%m-%dT%H:%M:%SZ')
                    date = datetime(int(dt.strftime('%Y')), int(dt.strftime('%m')), int(dt.strftime('%d')))
                    if "all" in saving_dict.keys():
                        # save time and clicks together
                        saving_dict['all'].append((date,meta_data["clicks"]))
                    else:
                        saving_dict['all'] = [(date,meta_data["clicks"])]

                elif what == "wordcloud":
                    if meta_data["text"]:
                        for values in meta_data["text"]:
                            if values[3] in saving_dict:
                                saving_dict[values[3]].append(int(meta_data["clicks"]))
                            else:
                                saving_dict[values[3]] = [int(meta_data["clicks"])]

        return saving_dict

    def plot_clicks(self, what, savingpath, dict, type = "mean"):
        if what == "color":
            if type == "mean":
                meanclicks_colors = {}
                # Iterate threw the colors which have likes and frequencies in there
                for color in dict.keys():
                    denominator = 0 # denominator for the mean
                    clicks = 0 # sum of all clicks
                    for clicks_freq in dict[color]:
                        clicks += int(clicks_freq[0]) * clicks_freq[1]
                        denominator += clicks_freq[1]
                    meanclicks_colors[color] = clicks/denominator
                colors = [int(colors) for colors in meanclicks_colors.keys()]
                mean = [means for means in meanclicks_colors.values()]

                # plot
                for_colors = [hsv_to_rgb([i/179,1,1]) for i in colors]
                plt.figure(figsize = (12,6))
                plt.bar(colors, mean, color = for_colors)
                plt.title("Clicks with colors", size = 15)
                plt.xlabel("Color in HSV")

            if type == "median":
                '''
                clicks = {}
                counter = 0
                for color in dict.keys():
                    print("Color_counter:", counter, end='\r')
                    counter += 1
                    for clicks_freq in dict[color]:
                        for freq in range(clicks_freq[1]):
                            if color in clicks.keys():
                                clicks[color].append(int(clicks_freq[0]))
                            else:
                                clicks[color] = [int(clicks_freq[0])]
                    clicks[color] = np.median(np.array(clicks[color]))

                colors = [int(colors) for colors in clicks.keys()]
                median = [median for median in clicks.values()]
                '''
                #'''
                #OTHER WAY:
                counter = 0
                number_of_pixels = 0
                way_to_half_pixels = 0
                colors = []
                median = []
                clicks_array = []
                for color in dict.keys(): # Go threw all colors
                    print("Color_counter:", counter, end ="\r")
                    number_of_pixels = 0
                    way_to_half_pixels = 0
                    median_found = False
                    counter += 1
                    for clicks_freq in dict[color]: # read out number of total pixels and get all the click
                        number_of_pixels += clicks_freq[1]
                        if int(clicks_freq[0]) in clicks_array:
                            continue
                        else:
                            clicks_array.append(int(clicks_freq[0]))
                    clicks_array.sort()
                    half_of_pixels = int(number_of_pixels/2)
                    for clicks in clicks_array:
                        indices = np.where(np.array(dict[color])[:,0] == clicks)
                        for index in list(indices):
                            if len(index) > 0:
                                way_to_half_pixels += dict[color][index[0]][1]
                                if way_to_half_pixels > half_of_pixels:
                                    median.append(clicks)
                                    median_found = True
                        if median_found:
                            break

                    colors.append(int(color))

                print("\n",len(median))
                #'''
                for_colors = [hsv_to_rgb([i/179,1,1]) for i in colors]
                plt.figure(figsize = (12,6))
                plt.bar(colors, median, color = for_colors)
                plt.xlabel("Color in HSV")


        elif what == "emotion":
            # I will calculate here the clicks prediction based on detected emotion probability
            expected_clicks = [0]*len(dict.keys())
            weight = [0]*len(dict.keys())
            emotions = []
            number_of_thumbnails = []
            for i, emotion in enumerate(dict.keys()):
                for clicks, percentage in dict[emotion]:
                    expected_clicks[i] += int(clicks)*percentage/100
                    weight[i] += percentage/100
                emotions.append(emotion)

            # plot
            fig, ax = plt.subplots(figsize = (12,6))
            bar_plot = plt.bar(emotions, np.array(expected_clicks)/np.array(weight), color = 'lightblue')
            plt.title("Clicks with emotions", size = 15)
            plt.xlabel("type of emotion")


        elif what == "face_count":
            if type == "mean":
                meanclicks_face_count = []
                number_of_faces = []
                number_of_thumbnails = []
                for face_number in dict.keys():
                    denominator = 0 # denominator for the mean
                    clicks = 0 # sum of all clicks
                    for clicks_number in dict[face_number]:
                        clicks += int(clicks_number)
                        denominator += 1
                    meanclicks_face_count.append(clicks/denominator)
                    number_of_faces.append(face_number)
                    number_of_thumbnails.append(len(dict[face_number]))
                fig, ax = plt.subplots(figsize = (12,6))
                #plt.figure(figsize = (12,6))
                #colors = ['']
                bar_plot = plt.bar(number_of_faces, meanclicks_face_count, color = 'lightblue')
                for idx,rect in enumerate(bar_plot):
                    height = rect.get_height()
                    ax.text(rect.get_x() + rect.get_width()/2., height*0.5,
                            number_of_thumbnails[idx],
                            ha='center', va='bottom', rotation=90)
                plt.xlabel("number of faces")
                plt.title("Clicks with face count", size=15)

            if type == "median":
                number_of_faces = []
                median_clicks = []
                for face_number in dict.keys():
                    median_clicks.append(np.median(np.array(dict[face_number])))
                    number_of_faces.append(face_number)
                plt.figure(figsize = (12,6))
                plt.bar(number_of_faces, median_clicks)
                plt.xlabel("number of faces")

        elif what == "face_size":
            size = []
            clicks = []
            for data in dict['all']:
                size.append(data[0])
                clicks.append(int(data[1]))
            plt.figure(figsize = (12,6))
            plt.scatter(size, clicks, s = 1)
            plt.yscale("log")
            plt.xlabel("size of faces")
            plt.title("Clicks with face size", size = 15)


        elif what == "duration":
            time = []
            clicks = []
            for data in dict['all']:
                time.append(data[0])
                clicks.append(int(data[1]))
            plt.figure(figsize = (12,6))
            plt.scatter(time, clicks, s = 1)
            plt.yscale("log")
            plt.xlabel("length of video")
            plt.title("Clicks with duration", size = 15)


        elif what == "date":
            date = []
            clicks = []
            for data in dict['all']:
                date.append(data[0])
                clicks.append(int(data[1]))
            plt.figure(figsize = (12,6))
            plt.plot_date(date, clicks, ms = 1)
            plt.yscale("log")
            plt.xlabel("date of release")
            plt.title("Clicks with release date", size = 15)

        plt.ylabel("clicks")
        plt.savefig(savingpath)
        plt.show()
        return  None

    def wordcloud_clicks(self, savingpath, dict):
        word_with_median_clicks = {}
        for words, clicks in dict.items():
            if len(clicks) > 499:
                word_with_median_clicks[words] = np.median(np.array(clicks))
        wc = WordCloud(width=1400, height=800, min_font_size = 10, background_color ='white', collocations=False).generate_from_frequencies(word_with_median_clicks)

        plt.figure(figsize=(14,8))
        plt.imshow(wc)
        plt.axis('off')
        plt.savefig(savingpath)
        plt.show()

    def general_plot(self, what):
        saving_dict = {}
        # Open the file where the thumbnails are saved
        with(open(self.ID)) as f:
            # go threw all of the thumbnails
            limits_clicks = [1e4, 1e5, 1e6, 1e7, 100e6, 1e20]
            amount = [0]*len(limits_clicks)
            clicks = []
            counter = 0
            for lines in f:
                counter += 1
                print(counter, end='\r')
                # load the meta_data of the thumbnail
                g = open(self.path+"thumbnail_"+lines[:-1]+".csv")
                meta_data = json.load(g)
                g.close()

                if what == "clicks":
                    clicks.append(int(meta_data["clicks"]))
                    '''
                    #for PIE CHART:
                    for idx, limit in enumerate(limits_clicks):
                        if int(meta_data["clicks"]) < limit:
                            amount[idx] += 1
                            print(amount)
                            print("doing Something")
                            break
                    '''

                elif what == "wordcloud":
                    if meta_data["text"]:
                        for values in meta_data["text"]:
                            if values[3] in saving_dict.keys():
                                saving_dict[values[3]] += 1
                            else:
                                saving_dict[values[3]] = 1

                elif what == "vid_per_channel":
                    if meta_data["channelId"] in saving_dict:
                        saving_dict["channelId"] += 1
                    else:
                        saving_dict["channelId"] = 1

        if what == "clicks":
            #print(amount)
            #return amount
            return clicks
        else:
            return saving_dict

    def plot(self, what, savingpath, dict_or_arr):
        if what == "clicks":

            plt.figure(figsize = (12,6))
            plt.xlabel("clicks")
            plt.ylabel("amount of videos")
            plt.xscale("log")
            plt.hist(dict_or_arr, 10000)
            plt.title("Click distribution", size = 15)
            plt.savefig(savingpath)
            '''
            # PIE CHART:
            limits_clicks = ['0 < 10k','10k < 100k', "100k < 1m", "1m < 10m", "10m < 100m", "> 100m"]
            print(dict_or_arr)
            fig = go.Figure(data=[go.Pie(labels=limits_clicks, values=dict_or_arr, pull=[0, 0.2, 0.1, 0 , 0.1,0.3], hole=.4)])
            fig.update_layout(title_text="Click Distribution of PewDiePie")
            fig.show()
            '''

        elif what == "wordcloud":
            wc = WordCloud(width=1400, height=800, min_font_size = 10, background_color ='white', collocations=False).generate_from_frequencies(dict_or_arr)

            plt.figure(figsize=(14,8))
            plt.imshow(wc)
            plt.axis('off')
            plt.savefig(savingpath)
            plt.show()

        elif what == "vid_per_channel":
            print(a)


################################################################################################
################################################################################################
###################################### main programm ###########################################
################################################################################################
################################################################################################

plots = meta_with_clicks("datafiles/MrBeast/thumbnail_IDS.txt", "datafiles/MrBeast/")

#plots.plot("wordcloud", "datafiles/MrBeast/Plots/wordcloud_freq.png", plots.general_plot("wordcloud"))
#plots.wordcloud_clicks("datafiles/MrBeast/Plots/wordcloud_clicks_500_cutoff.png", plots.likes_against_what("wordcloud"))
plots.plot("clicks", "datafiles/MrBeast/Plots/clicks_distribution_log.png", plots.general_plot("clicks"))

#plots.plot_clicks("color", "datafiles/MrBeast/Plots/clicks_colors_mean.png", plots.likes_against_what("color"))
#plots.plot_clicks("color", "datafiles/MrBeast/Plots/clicks_colors_median.png", plots.likes_against_what("color"), "median")
#plots.plot_clicks("emotion", "datafiles/MrBeast/Plots/clicks_emotions.png", plots.likes_against_what("emotion"))
#plots.plot_clicks("face_count", "datafiles/MrBeast/Plots/clicks_facecount_mean.png",  plots.likes_against_what("face_count"))
#plots.plot_clicks("face_count", "datafiles/MrBeast/Plots/clicks_facecount_median.png",  plots.likes_against_what("face_count"), "median")
#plots.plot_clicks("face_size", "datafiles/MrBeast/Plots/clicks_facesize.png", plots.likes_against_what("face_size"))
#plots.plot_clicks("duration", "datafiles/MrBeast/Plots/clicks_duration.png", plots.likes_against_what("duration"))
#plots.plot_clicks("date", "datafiles/MrBeast/Plots/clicks_releasedate.png", plots.likes_against_what("date"))
#VIDEOS PER channel!!!!!!!!!!!!!!!
