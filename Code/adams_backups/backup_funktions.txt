def clicks_with_colors_in_thumbnail(directory_path, filename_meta_data, number_of_bins, saving_name):

    IN:

    OUT:

    INFOS:


    #import all files in directory_path (right now I am just taking a little excerb with [:3])
    meta_data = json.load(open(directory_path+filename_meta_data))
    thumbnail_ids = [thumbnail_id for thumbnail_id in meta_data.keys() if "viewCount" in meta_data[thumbnail_id].keys()][:100]
    clicks = [meta_data[thumbnail_id]["viewCount"] for thumbnail_id in thumbnail_ids]

    ######################## This is just out of interest
    imagefiles_in_dir = [f for f in listdir(directory_path) if f[-3::]=='png']

    for i, thumbnail_id in enumerate(imagefiles_in_dir):
        if thumbnail_id[10:-4] not in thumbnail_ids:
            print(thumbnail_id[10:-4])

    ########################

    # make Image with cv2.imread() and then transform it into HSV values with cv2.cvtColor(), to create a list of image
    hsvImages = [cv2.cvtColor(cv2.imread(directory_path+"thumbnail_"+thumbnail_id+'.png'), cv2.COLOR_BGR2HSV) for thumbnail_id in thumbnail_ids ]

    #concat thumbnails as array of tuples(thumbnail, clicks)
    hsvImage_and_clicks = [(hsvImages[i], clicks[i]) for i in range(len(clicks))]
    print("Done with Image and Clicks in one Array!!\nStarting to put each pixel with a click in .txt file...")

    #put color in clicks into a txt file...
    with open(directory_path+saving_name+"_color_and_likes.txt", 'w') as f:
        print('pixel_color,clicks', file=f)
        for i in range(len(clicks)):
            print(i,end="\r")
            for pixel_color in hsvImage_and_clicks[i][0][:,:,0].flatten():
                print(pixel_color,',',clicks[i], file=f)

def color_and_clicks(directory_path_to_thumbnails, filename_meta_data, thumbnail_Id):

    # Load the meta_data file
    meta_data = json.load(open(directory_path+filename_meta_data))

    # check if the thumbnail even has a view count
        # if it has a view count: then do the processing
        # else: return None
    if "viewCount" in meta_data[thumbnail_Id].keys():
        # get clicks fo thumbnail/video
        clicks = meta_data[thumbnail_id]["viewCount"]

        # get image
        imagefile_in_dir = 'thumbnail_'+thumbnail_Id+'.png'

        # make hsv Image
        hsvImage = cv2.cvtColor(cv2.imread(imagefile_in_dir), cv2.COLOR_BGR2HSV)

        # put clicks and Image together
        hsvImage_and_clicks = (hsvImage, clicks)

        # write a dict with clicks and each color with its frequency
        color_freq_clicks = {}
        color_freq_clicks['clicks'] = clicks
        for pixel_color in hsvImage_and_clicks[0][:,:,0].flatten():
            if pixel_color in color_freq_clicks.keys():
                color_freq_clicks[pixel_color] += 1
            else:
                color_freq_clicks[pixel_color] = 1

        return color_freq_clicks

def show_cv2_image(images_in_rgb, rows=1, columns=None, title=None):
    # TODO:
    #   - handle overflow title
    #   - auto-set rows, columns

    '''
    IN:
        - takes list of images in cv2 readable format
    OUT:
        - shows picture(s)
    INFOS:
        Shows (multiple) picture(s side-by-side)

    '''

    if columns is None: columns = len(images_in_rgb)

    # Does not work with qt (QtAgg, QtCairo, Qt5Agg, Qt5Cairo)
    ## At least on my machine (David)
    #matplotlib.use('GTK4Agg')



    fig, ax = plt.subplots(rows, columns, figsize=(10,5))

    if columns > 1:
        if rows > 1:
            for i in range(rows):
                for j in range(columns):
                    ax[i][j].imshow(images_in_rgb[i*columns+j])
                    if title: ax[i][j].set_title(title[i*columns+j])
        else:
            for j in range(columns):
                ax[j].imshow(images_in_rgb[j])
                if title: ax[j].set_title(title[j])
    else:
        ax.imshow(images_in_rgb[0])
        if title: ax.set_title(title)

    fig.tight_layout()
    plt.show()
    return

def BiGfAtlOoP():
    # get meta_data
    filepath_metadata = "datafiles/PewDiePie/PewDiePie_meta.txt"
    meta_data = json.load(open(filepath_metadata))

    # filter out the data that has a viewCount
    thumbnail_Ids = [thumbnail_Id for thumbnail_Id in meta_data.keys() if "viewCount" in meta_data[thumbnail_Id].keys()][:10]

    for thumbnail_Id in thumbnail_Ids:
        f = open("datafiles/PewDiePie/"+thumbnail_Id+".csv", "w")

        thumbnail_dict = color_and_clicks("datafiles/PewDiePie/", meta_data[thumbnail_Id], thumbnail_Id)
        thumbnail_dict["emotion"] = detect_emotions("datafiles/PewDiePie/thumbnail_"+thumbnail_Id+".png", show_results=False)

        json.dump(thumbnail_dict, f)
        f.close()
        del meta_data[thumbnail_Id]
