#!pip3 install deepface easyocr textblob


################################################################################################
################################################################################################
################################### imports and globals ########################################
################################################################################################
################################################################################################



import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import json, os
from os import listdir
from os.path import isfile, join
import sys
os.environ['KMP_DUPLICATE_LIB_OK']='True'
# Turn off those annoying log messages
## Hmmm, the status bar is still hanging 'round..
'''
0 = all messages are logged (default behavior)
1 = INFO messages are not printed
2 = INFO and WARNING messages are not printed
3 = INFO, WARNING, and ERROR messages are not printed
'''
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


import keras
import tensorflow as tf

from tensorflow.keras.preprocessing import image

import cv2
from deepface import DeepFace
from deepface.commons import functions
from deepface.detectors import FaceDetector

import easyocr
from textblob import TextBlob
from textblob import Word

#from spellchecker import SpellChecker

import pkg_resources

import re
import nltk
nltk.download('wordnet')
nltk.download('omw-1.4')
## or python -m textblob.download_corpora

################################################################################################
################################################################################################
######################################### functions ############################################
################################################################################################
################################################################################################

    # misc functions

def show_cv2_image(images_in_rgb, rows=1, columns=None, title=None):
    # TODO:
    #   - handle overflow title
    #   - auto-set rows, columns

    '''
    IN:
        - takes list of images in cv2 readable format
    OUT:
        - shows picture(s)
    INFOS:
        Shows (multiple) picture(s side-by-side)

    '''

    if columns is None: columns = len(images_in_rgb)

    # Does not work with qt (QtAgg, QtCairo, Qt5Agg, Qt5Cairo)
    ## At least on my machine (David)
    #matplotlib.use('GTK4Agg')



    fig, ax = plt.subplots(rows, columns, figsize=(10,5))

    if columns > 1:
        if rows > 1:
            for i in range(rows):
                for j in range(columns):
                    ax[i][j].imshow(images_in_rgb[i*columns+j])
                    if title: ax[i][j].set_title(title[i*columns+j])
        else:
            for j in range(columns):
                ax[j].imshow(images_in_rgb[j])
                if title: ax[j].set_title(title[j])
    else:
        ax.imshow(images_in_rgb[0])
        if title: ax.set_title(title)

    fig.tight_layout()
    plt.show()
    return


def dict_to_str(dictionary):

    out_str = ""

    for key in dictionary.keys():
        out_str += str(key) + " : " + str(dictionary[key]) + "\n"

    return out_str



    # data prep functions

def load_video_data(directory_path, meta_data_file):
    '''
    IN:
        - directory_path <string>: to directory of data
    OUT:
        - meta_data <dic>: all the data in one dictionary
    INFOS:
        - loads and puts thumbnails and metadata of videos in one dictionary
    '''
    f = open(directory_path+meta_data_file)
    meta_data = json.load(f)
    f.close()
    i_max = len(meta_data)
    for i, key in enumerate(meta_data):
        print(f"load video {i}/{i_max}", end="\r")
        img = tf.keras.utils.load_img(directory_path+"thumbnail_"+key+".png")
        meta_data[key]["image"] = tf.keras.preprocessing.image.img_to_array(img)
    print("\nloading videos done")


    return meta_data


def structure_meta_data(meta_data):
    '''
    IN:
        - meta_data

    OUT:
        - transform_data (probably a array of arrays or something like that)

    INFOS:
        - give metadata object and return data in a form that is understandable for the neural network

    '''
    return transform_data


def color_and_clicks(directory_path_to_thumbnails, meta_data, thumbnail_Id):
    '''
    IN:
        - directory_path_to_thumbnails <string>: directory where thumbnails and metadata are
        - meta_data <dict>: meta_data of a specific thumbnail_Id
        - thumbnail_Id <string>: thumbnail that should be looked at

    OUT:
        - color_freq_clicks <dict>: dict of color with frequency and clicks

    INFOS:
        - ONE IDEA
            to iterate threw all of the thumbnails, use:

                imagefiles_in_dir = [f for f in listdir(directory_path) if f[-3::]=='png']

            and then iterate threw the imagefiles_in_dir array, but only send over the thumbnail_Id:

                for imagefile in imagefiles_in_dir:
                    color_and_clicks("BLAH", "BLUB", imagefile[10:-4])

    '''
    # get clicks fo thumbnail/video
    clicks = meta_data["viewCount"]

    # get image
    imagefile_in_dir = 'thumbnail_'+thumbnail_Id+'.png'

    # make hsv Image
    hsvImage = cv2.cvtColor(cv2.imread(directory_path_to_thumbnails+imagefile_in_dir), cv2.COLOR_BGR2HSV)

    # put clicks and Image together
    hsvImage_and_clicks = (hsvImage, clicks)

    # write a dict with clicks and each color with its frequency
    color_freq_clicks = {}
    color_freq_clicks['clicks'] = clicks

    for pixel_color in hsvImage_and_clicks[0][:,:,0].flatten():
        if str(pixel_color) in color_freq_clicks.keys():
            color_freq_clicks[str(pixel_color)] += 1
        else:
            color_freq_clicks[str(pixel_color)] = 1
    return color_freq_clicks



    ## emotions ##

def emotion_prediction(face):
    # TODO:

    '''
    IN:
        - Image (already detected face! -> mostly cropped version of larger image) as loaded with cv2.imread(<PATH>): <np.array(<np.array(<float>)>, <np.array(<float>)>, <np.array(<float>)>, <np.array(<float>)>)>
    OUT:
        - List containing the predicted results for each emotion ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']: <list(<float>)>
    INFOS:
        Predict emotions in already detected face.

    based upon https://github.com/serengil/deepface/blob/44855ed29ae5d96748e5a1b2cf8404ef8122ce40/deepface/DeepFace.py#L397
    '''

        # Image preprocessing

    # ref: https://github.com/serengil/deepface/blob/44855ed29ae5d96748e5a1b2cf8404ef8122ce40/deepface/commons/functions.py#L172

    faceGray = cv2.cvtColor(face.copy(), cv2.COLOR_BGR2GRAY)

    # Resize image to expected shape

    # img = cv2.resize(img, target_size) #resize causes transformation on base image, adding black pixels to resize will not deform the base image

    target_size = (48, 48)

    factor_0 = target_size[0] / faceGray.shape[0]
    factor_1 = target_size[1] / faceGray.shape[1]
    factor = min(factor_0, factor_1)

    dsize = (int(faceGray.shape[1] * factor), int(faceGray.shape[0] * factor))
    faceGray = cv2.resize(faceGray, dsize)

    # Then pad the other side to the target size by adding black pixels
    diff_0 = target_size[0] - faceGray.shape[0]
    diff_1 = target_size[1] - faceGray.shape[1]


    # Double check: if target image is not still the same size with target.
    if faceGray.shape[0:2] != target_size:
        faceGray = cv2.resize(faceGray, target_size)


    # Normalizing the image pixels

    img_pixels = image.img_to_array(faceGray) #what this line doing? must?
    img_pixels = np.expand_dims(img_pixels, axis = 0)
    img_pixels /= 255 #normalize input in [0, 1]


        # Predict emotions

    models = {}
    models['emotion'] = DeepFace.build_model('Emotion')

    emotion_predictions = models['emotion'].predict(img_pixels)[0,:]

    sum_of_predictions = emotion_predictions.sum()


        # Postprocessing

    #resp_obj = {}
    #resp_obj["emotion"] = {}
    #emotion_labels = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']

    ### change if append faster than acces and change
    emotions = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']

    for i in range(0, len(emotions)):
        #emotion_label = emotion_labels[i]
        emotion_prediction = 100 * emotion_predictions[i] / sum_of_predictions
        #resp_obj["emotion"][emotion_label] = emotion_prediction

        #emotions.append(emotion_prediction)
        emotions[i] = emotion_prediction

    #resp_obj["dominant_emotion"] = emotion_labels[np.argmax(emotion_predictions)]

    #return resp_obj
    return emotions


def detect_emotions(image_path, show_results=False):

    #TODO:

    '''
    IN:
        - Path to image: <str>
        - optionally show results with matplotlib <bool>
    OUT:
        - List of x, y - Position (top left corner of rectangle), area and predictions for different emotions ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']
            for each detected face: <list(<list(<float>)>)>
    INFOS:
        Detect face(s) in given image and return the predicted emotions.

    based upon https://geekyhumans.com/emotion-detection-using-python-and-deepface/
    '''

    # Load image
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    """
    #### only detects _a single_ face : https://github.com/serengil/deepface/blob/44855ed29ae5d96748e5a1b2cf8404ef8122ce40/deepface/detectors/FaceDetector.py#L35 ....
    faces = DeepFace.analyze(image, actions=['emotion'])
    print(faces)
    #"""

        # Detect (and return) faces in image

    detector_backend = 'opencv'
    face_detector = FaceDetector.build_model(detector_backend)

    # Get (_only_) detected faces with respective region
    ## maybe get some grayscale for even better performance?
    #gray=cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = FaceDetector.detect_faces(face_detector, detector_backend, image, align=False)

    ## https://github.com/serengil/deepface/blob/44855ed29ae5d96748e5a1b2cf8404ef8122ce40/deepface/commons/realtime.py#L143

    features = []
    x_norm = 640
    y_norm = 480
    area_norm = x_norm*y_norm

    for face, (x, y, w, h) in faces:

            # Get emotion
            emotions = emotion_prediction(face)
            features += [[x/x_norm, y/y_norm, w*h/area_norm] + emotions]


            # Draw rectangle around detected face to main image
            if show_results: cv2.rectangle(image, (x,y), (x+w,y+h), (67,67,67), 1)

            ## needs some adjustment
            #cv2.putText(image, dict_to_str(emotion), (int(x+w/4),int(y+h/1.5)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)


        # Show results
    if show_results:

        images_to_show = [image]
        image_titles = [None]

        for j, (face, _) in enumerate(faces):
            images_to_show.append(face)

            emotion_labels = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']
            dominant_emotion = emotion_labels[np.argmax(features[j][3:])]

            image_titles.append(dominant_emotion)
            print(dominant_emotion + ": ", features[j])

        show_cv2_image(images_to_show, title=[*image_titles])

    return features



    ## text ##

# a function to load our vector model as dictionary
def load_glove_model(File):
    '''
    IN:
        - File <string>: Path to file with vector encoding
    OUT:
        - glove_model <dict(<str>, <np.array(<float>)>)>: dictionary with 100d vector from GloVe dict
    INFOS:
        - Load vector encoding to dictionary and update spelling_list with GloVe words, numbers (and miscs?)
    '''
    glove_model = {}
    with open(File, 'r', encoding="utf8") as f:
        for line in f:
            split_line = line.split()
            word = split_line[0]
            embedding = np.array(split_line[1:], dtype=np.float64)
            glove_model[word] = embedding
    return glove_model


def update_wordlist(path_to_wordlist, update_list, silent=False):

    '''
    IN:
        - File <string>: Path to file with wordlist
        - update_list <list(<str>)>: list with words which should be included in wordlist
        - (otional) silent <bool>: if True: print no string
    OUT:
        - 0 if successfull
    INFOS:
        - Append new words with frequency 1 to wordlist file at given PATH.
    '''

    if not silent: print("Updating wordlist at "+path_to_wordlist)

    # Ignore already known words
    with open(path_to_wordlist, 'r', encoding="utf8") as f:

        lines = f.readlines()
        number_words = len(lines)
        for j, word in enumerate(lines):
            if not silent: print(f"Already checked {j/number_words*100:.2f}% of list", end="\r")

            # Every line contains <word> <frequency>
            word = word.split()[0]
            if word in update_list:
                update_list.remove(word)

    # Append new words to wordlist file (if there are any)
    number_new_words = len(update_list)

    if not silent: print(f"\nTotal new words: {number_new_words}")
    if number_new_words > 0:
        with open(path_to_wordlist, 'a', encoding="utf8") as f:
            for j, word in enumerate(update_list):
                if not silent: print(f"Already updated {j/number_new_words*100:.2f}% of list", end="\r")
                # Set frequency to 1 (abitrary)
                f.write(word + " 1\n")
    return 0


def analyse_word(word, initial_confidence=1, vector_dictionary=load_glove_model('../glove.6B.100d.txt'), printout=False):
    '''
    IN:
        - word <str>                                            Word to analyse
        - vector_dictionary <dict(<str>:<np.array(<float>)>)>:  Dictionary containing the vector encoding for all words
        - printout <bool>:                                      Toggle for stdout updates (default False: off)
    OUT:
        - <tuple(<str>, <float>, <float>, <float>, <np.array(<float>)>)>: Tuple containing the corrected and lemmatized word, corresonding confidence, sentiment.polarity, sentiment.subjectivity, 100d vector encoding
    INFOS:
        Analyse given word with textblob: correct spelling (textblob wordlist enhanced with GloVe words) and lemmatize, update confidence score, get sentiment (polarity, subjectivity), get GloVe (default) 100d encoding.
    '''


    """
    ## Keep as reference atm

    print("\n\n SpellChecker:\n\n")

    #spellchecker = SpellChecker()
    # check words and find closest match
    ## https://pypi.org/project/pyspellchecker/

    # find those words that may be misspelled
    detected_words = words[1].split()
    misspelled = spellchecker.unknown(detected_words)

    print("detected: ", detected_words, "; misspelled: ", misspelled)

    for word in misspelled:
        # Get the one `most likely` answer
        corrected_word = spellchecker.correction(word)

        # Get a list of `likely` options
        print("correction: ", corrected_word, spellchecker.candidates(word))
    #"""


    # check words and find closest match
    ## https://textblob.readthedocs.io/en/dev/quickstart.html#spelling-correction

    word = word.lower() #"".join(re.findall("[a-zA-Z0-9#]+", word))

    # Get the one `most likely` answer
    candidates = Word(word).spellcheck()
    corrected_word = candidates[0]

    # Get a list of `likely` options
    if printout: print("correction: ", corrected_word, candidates)

    # Confidence we have in a textblob classification
    p_textblob = 0.7

    # Even if word not in dict and no alternative found -> calculate updated confidence with textblob confidence ('arbitrarily' set)
    if corrected_word[1] == 0: corrected_word = (corrected_word[0], p_textblob)



    # Sentiments analysis on word level -- normally not what you want...!?
    ## maybe naivebayesanalyzer?!?
    word = TextBlob(corrected_word[0])
    sentiment = (word.sentiment.polarity, word.sentiment.subjectivity)

    if printout: print("sentiment: ", sentiment)

    # Get vector encoding
    try:
        vector = vector_dictionary[str(word)]
    except KeyError:
        # Word not in dictionary -> set vector to all zeros
        vector = np.zeros((100))


    ## maybe add global frequency in some way?!
    return (str(Word(word).lemmatize()), corrected_word[1]*initial_confidence, *sentiment, *vector)





def detect_text(image_path, vector_dictionary=load_glove_model('../glove.6B.100d.txt'), gpu_available=False, show_results=False):

    ## not_test

    #TODO:
    #   - get better dictionary for textblob (include blitz, numericals, ..?)

    '''
    IN:
        - Path to image: <str>
        - optionally improve detection time with gpu (if available): <bool>
        - optionally show results with matplotlib: <bool>
    OUT:
        -  List x, y - Position of detected region (top left corner of rectangle), area and predicted words with confidence, sentiment.polarity, sentiment.subjectivity, 100d vector encoding: <list(<list(<float>, <float>, <float>, <str>, <float>, <float>, <float>, <np.array(<float>)>)>, )>
    INFOS:
        Detect words in given image and return prediction.

    '''

    features = []

        # EasyOCR

    # Text detection and extraction
    ## https://www.jaided.ai/easyocr/documentation/
    reader = easyocr.Reader(["en"], gpu=gpu_available, verbose=False)

    ## play around with params!
    extracted_text = reader.readtext(image_path)#rotation_info=90, 180 ,270] #detail=0, paragraph=True)



        # Postprocessing
    ## maybe infere from image
    x_norm = 640
    y_norm = 480
    area_norm = x_norm*y_norm

    for words in extracted_text:

        if show_results: print("\n--------------------------\n")
        if show_results: print("\n ", words)


        #"""

        ### needs some refinement! : add numericals,... to dict

        # Get different words
        detected_words = words[1].split()

        if show_results: print("detected: ", detected_words)

        ## hmmm spellchecker was a bit better....
        for j, word in enumerate(detected_words):
            detected_words[j] = analyse_word(word, words[2], vector_dictionary, show_results)


        #"""

        if show_results: print("\n--------------------------\n")


        # create feature vector

        # top left corner x, y,
        xtl, ytl = words[0][0]
        # A = width*height
        area = abs(xtl - words[0][2][0]) * abs(ytl - words[0][2][1])

        for word in detected_words:
<<<<<<< HEAD
            #
=======
>>>>>>> 42b7397ffc0225939ab1bd21b7f14894bb5f01fe
            features.append([xtl/x_norm, ytl/y_norm, area/area_norm, *word])


        # Show results
    if show_results:
        # Read image from which text got extracted
        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        for words in extracted_text:
            p1, _, p3, _ = words[0]
            rect = cv2.rectangle(image, p1, p3, (0, 255, 0), 2)

        print(features)

        show_cv2_image([image])

    return features



def BiGfAtlOoP(how_to_write, update_words=False):
    """
        LOOP TO GET ALL THE DATA!
    """
    # get meta_data
    filepath_metadata = "datafiles/MrBeast/MrBeast_clustering_meta.txt"
    meta_data = json.load(open(filepath_metadata))

    # filter out the data that has a viewCount
    thumbnail_Ids = [thumbnail_Id for thumbnail_Id in meta_data.keys() if "viewCount" in meta_data[thumbnail_Id].keys()]
<<<<<<< HEAD
    g = open("datafiles/PewDiePie/thumbnail_IDS.txt", how_to_write)
    i_maxlength = len(thumbnail_Ids)
=======
    g = open("datafiles/MrBeast/thumbnail_IDS.txt", how_to_write)
    i_maxlength = len(thumbnail_Ids[45683:])
>>>>>>> 42b7397ffc0225939ab1bd21b7f14894bb5f01fe


    # Just load the dict once for text processing
    glove_dict = load_glove_model('../glove.6B.100d.txt')
    # Update the standard textblob wordlist (https://raw.githubusercontent.com/sloria/TextBlob/dev/textblob/en/en-spelling.txt)

    #####
    ## ___SET YOUR OWN PATH___ !!!
    ####
    ## maybe add channel names, - split, ..?? numerics? -> might be already included in GloVe!?

    if update_words: update_wordlist("/home/cloud/.local/lib/python3.10/site-packages/textblob/en/en-spelling.txt", [*glove_dict])# + [str(j) for j in range(10)])

    for i, thumbnail_Id in enumerate(thumbnail_Ids[45683:]):

        thumbnail_id_file = "datafiles/MrBeast/thumbnail_"+thumbnail_Id
        print(f'We are at step {i+1}/{i_maxlength}')
        f = open(thumbnail_id_file+".csv", how_to_write)


        title = meta_data[thumbnail_Id]["title"]
        analysed_title = []
        for word in title.split():
            analysed_title.append([*analyse_word(word)])

        thumbnail_dict = color_and_clicks("datafiles/MrBeast/", meta_data[thumbnail_Id], thumbnail_Id)
        thumbnail_dict["title"] = [title, analysed_title]
        thumbnail_dict["publishedAt"] = meta_data[thumbnail_Id]["publishedAt"]
        thumbnail_dict["duration"] = meta_data[thumbnail_Id]["duration"]
        thumbnail_dict["channelId"] = meta_data[thumbnail_Id]["channelId"]
        thumbnail_dict["emotion"] = detect_emotions(thumbnail_id_file+".png", show_results=False)
        thumbnail_dict["text"] = detect_text(thumbnail_id_file+".png", vector_dictionary=glove_dict, show_results=False, gpu_available=True)
        print(thumbnail_dict)
        json.dump(thumbnail_dict, f)
        print(thumbnail_Id, file=g)
        f.close()
        del meta_data[thumbnail_Id]
    g.close()

################################################################################################
################################################################################################
###################################### main programm ###########################################
################################################################################################
################################################################################################

### thumbnail_0S3Pp45zJfI.png ## O K . . .

#------------------------------- BIG FAT LOOP -------------------------------------------------#

BiGfAtlOoP("a", update_words=False)


#----------------------------------- END ------------------------------------------------------#

################################################################################################
################################################################################################
########################################### TESTS ##############################################
################################################################################################
################################################################################################


#detect_emotions("/home/stuser/Desktop/ManicureLearning/Code/datafiles/emoji2.jpeg", show_results=True)# even emojiis ! (though not always the right emotion - I (David) think)

#detect_emotions("/home/stuser/Desktop/ManicureLearning/Code/datafiles/PewDiePie/thumbnail_0FH0k_NZV6U.png", show_results=True)
#thumbnail_0Pl_m4IB9Mc.png", show_results=True) ## works!


#detect_text("./datafiles/sample4.jpg", show_results=True)#videodata/thumbnail_0Fy8SLaP-Mc.png", show_results=True)## kinda works..
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/videodata/thumbnail_0tZCQ25fsoI.png", show_results=True) ## at least trying...
#detect_text("datafiles/videodata/thumbnail_0UnHma-Y5Ec.png", show_results=True)
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/videodata/thumbnail_0priMq1ukao.png", show_results=True)# nope --> adjust hyperparams... or maybe delete some information? like faces/objects etc..? - WOW (EASYOCR)
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/PewDiePie/thumbnail_0eP_Kr3VQcY.png", show_results=True)# more or less!?
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/PewDiePie/thumbnail_0ERp2aVIHTA.png", show_results=True)# okok
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/PewDiePie/thumbnail_0FIG0EXmK94.png", show_results=True)# well - ok
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/PewDiePie/thumbnail_0FXkERWuyG0.png", show_results=True)# pretty much damn
#detect_text("/home/stuser/Desktop/ManicureLearning/Code/datafiles/PewDiePie/thumbnail_0Fy8SLaP-Mc.png", show_results=True)# nice (+-)
#test: for testing: langer satz: cmOZNxxGrJU, toyota: XQ93AQ2uCBM
#print('Color and Clicks:\n')
#meta_data = json.load(open("datafiles/PewDiePie/PewDiePie_meta.txt"))
#print(color_and_clicks("datafiles/PewDiePie/", meta_data['fqEyA20_-Oc'], 'fqEyA20_-Oc'))
#print('\n\nDetect emotions:\n')
#print(detect_emotions("datafiles/PewDiePie/thumbnail_-73iXbNNZ6o.png", show_results=True)))
#print('\n\nDetect text:\n')
#print(detect_text("datafiles/PewDiePie/thumbnail_-73iXbNNZ6o.png", show_results=True) )
