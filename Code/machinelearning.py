import numpy as np
import matplotlib.pyplot as plt
import json, os
from os import listdir
from os.path import isfile, join
import sys

import keras
import tensorflow as tf

from tensorflow.keras.preprocessing import image


from tensorflow.keras import layers
from tensorflow.keras import Model
from sklearn.model_selection import train_test_split
from tensorflow.keras.optimizers import Adam
from keras.losses import categorical_crossentropy
from sklearn.utils import shuffle
from tensorflow.keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, EarlyStopping


from datetime import datetime
import re

from numpy.random import seed
seed_nbr=7
seed(seed_nbr)
tf.random.set_seed(2)


########################################################################################
# !!!! run: export PYTHONHASHSEED=0 in console before script!!!!!
########################################################################################
print("\n\n!!!! run: export PYTHONHASHSEED=0 in console before script!!!!!\n\n")

#sys.path.append("/usr/local/lib/python3.8/site-packages")
#sys.path.append("/Users/Theo/Library/Python/3.8/lib/python/site-packages")
#import onnx
#import onnx2keras
#import torch


class CustomDataGen(tf.keras.utils.Sequence):
    '''
    Custom Data Generator function which can load thumbnails and video-meta-data batchwise
    '''

    def __init__(self, video_ids, data_directory,
                 batch_size,
                 input_size=(480, 640, 3),
                 shuffle=True,
                 rescale_img_by=1./255.,
                 full_net=False,
                 mean=0,
                 std=1):
        if shuffle:
            np.random.shuffle(video_ids)
            self.video_ids = video_ids
        else:
            self.video_ids = video_ids
        self.batch_size = batch_size
        self.input_size = input_size
        self.data_directory = data_directory
        self.rescale_img_by = rescale_img_by
        self.shuffle = shuffle
        self.full_net = full_net

        self.n = len(self.video_ids)

        self.mean = mean
        self.std = std


        return

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.video_ids)
        return


    def __get_data(self, video_ids_batch): # Generates data containing batch_size samples
        meta_data_shape = (self.batch_size, meta_data_space)
        meta_data_batch = np.zeros(meta_data_shape)

        thumbnail_shape = (self.batch_size, *self.input_size)
        thumbnail_batch = np.empty(thumbnail_shape)

        output_shape = (self.batch_size, 1)
        output_batch = np.empty(output_shape)

        for i, video_id in enumerate(video_ids_batch):
            # load metadata from json
            f = open(self.data_directory + "thumbnail_" + video_id + ".csv")
            meta_data_video = json.load(f)
            f.close()

            if self.full_net:
                # load prominent colors
                meta_data_batch[i][:number_of_hues] = np.array([meta_data_video[str(i)] if str(i) in meta_data_video else 0 for i in range(176)])/self.input_size[0]/self.input_size[1]
                # load title words
                words = np.array(meta_data_video["title"][1])
                n_words = len(words)
                if n_words > number_of_title_words:
                    n_words = number_of_title_words
                meta_data_batch[i][number_of_hues:number_of_hues+n_words*title_word_space] = words[:n_words, 1:].flatten()
                # text in thumbnail
                words = np.array(meta_data_video["text"])
                n_words = len(words)
                if n_words > number_of_thumbnail_words:
                    n_words = number_of_thumbnail_words
                meta_data_batch[i][number_of_hues+title_words_space:number_of_hues+title_words_space+n_words*thumbnail_word_space] = np.delete(words[:n_words].flatten(), list(range(3,n_words*(thumbnail_word_space+1),thumbnail_word_space+1)))
                # faces in thumbnail
                words = meta_data_video["emotion"]
                n_words = len(words)
                if n_words > number_of_faces:
                    n_words = number_of_faces
                meta_data_batch[i][number_of_hues+title_words_space+thumbnail_words_space:number_of_hues+title_words_space+thumbnail_words_space+n_words*space_per_face] = np.array(meta_data_video["emotion"]).flatten()[:n_words*space_per_face]
                # publishing time, duration, channel
                meta_data_batch[i][number_of_hues+title_words_space+thumbnail_words_space+emotion_space] = datetime.strptime(meta_data_video["publishedAt"], "%Y-%m-%dT%H:%M:%SZ").timestamp()/datetime(2022, 8, 2).timestamp()
                #print(meta_data_video["duration"], re.search(f"PT(\d+H)?(\d+M)?(\d+S)?", meta_data_video["duration"]))
                try:
                    words = np.array([ 0 if element==None else int(element[:-1]) for element in re.search(f"PT(\d+H)?(\d+M)?(\d+S)?", meta_data_video["duration"]).groups()])
                except Exception as errormessage:
                    print("no video length, pobably ongoing livestream (if duration P0D) - continue with videolength 0")
                    print("duration is: " + meta_data_video["duration"])
                    print(errormessage)
                    words = 0
                words = (words*np.array([60*60,60,1])).sum()
                if words > max_video_length:
                    words = max_video_length
                meta_data_batch[i][number_of_hues+title_words_space+thumbnail_words_space+emotion_space+1] = words/max_video_length

                meta_data_batch[i][number_of_hues+title_words_space+thumbnail_words_space+emotion_space+1+1] = int(str(hash(meta_data_video["channelId"]))[1:13])/10**12



            # load thumbnail
            img = tf.keras.utils.load_img(self.data_directory +"thumbnail_"+video_id+".png")
            thumbnail_batch[i] = tf.keras.preprocessing.image.img_to_array(img)*self.rescale_img_by

            # set output (views) value
            value = int(meta_data_video["clicks"])
            if value < 1:
               value = 1
            output_batch[i] = (np.log(value) - self.mean)/self.std


        if self.full_net:
            return tuple([thumbnail_batch, meta_data_batch]), tuple([output_batch,])
        else:
            return thumbnail_batch, tuple([output_batch,])

    def __getitem__(self, index):
        video_ids_batch = self.video_ids[index * self.batch_size:(index + 1) * self.batch_size]
        X, y = self.__get_data(video_ids_batch)
        return X, y

    def __len__(self):
        return self.n // self.batch_size


class BatchEarlyStopping(tf.keras.callbacks.Callback): # TAKEN FROM https://stackoverflow.com/questions/57618220/how-we-can-define-early-stopping-for-keras-to-check-after-each-batch-not-entire -> for batch early stopping
    """Stop training when a monitored quantity has stopped improving.
    # Arguments
        monitor: quantity to be monitored.
        min_delta: minimum change in the monitored quantity
            to qualify as an improvement, i.e. an absolute
            change of less than min_delta, will count as no
            improvement.
        patience: number of batches with no improvement
            after which training will be stopped.
        verbose: verbosity mode.
        mode: one of {auto, min, max}. In `min` mode,
            training will stop when the quantity
            monitored has stopped decreasing; in `max`
            mode it will stop when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        baseline: Baseline value for the monitored quantity to reach.
            Training will stop if the model doesn't show improvement
            over the baseline.
        restore_best_weights: whether to restore model weights from
            the batch with the best value of the monitored quantity.
            If False, the model weights obtained at the last step of
            training are used.
    """

    def __init__(self,
                 monitor='val_loss',
                 min_delta=0,
                 patience=0,
                 verbose=0,
                 mode='auto',
                 baseline=None,
                 restore_best_weights=False):
        super(BatchEarlyStopping, self).__init__()

        self.monitor = monitor
        self.baseline = baseline
        self.patience = patience
        self.verbose = verbose
        self.min_delta = min_delta
        self.wait = 0
        self.stopped_batch = 0
        self.restore_best_weights = restore_best_weights
        self.best_weights = None

        if mode not in ['auto', 'min', 'max']:
            warnings.warn('BatchEarlyStopping mode %s is unknown, '
                          'fallback to auto mode.' % mode,
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
        elif mode == 'max':
            self.monitor_op = np.greater
        else:
            if 'acc' in self.monitor:
                self.monitor_op = np.greater
            else:
                self.monitor_op = np.less

        if self.monitor_op == np.greater:
            self.min_delta *= 1
        else:
            self.min_delta *= -1

    def on_train_begin(self, logs=None):
        # Allow instances to be re-used
        self.wait = 0
        self.stopped_batch = 0
        if self.baseline is not None:
            self.best = self.baseline
        else:
            self.best = np.Inf if self.monitor_op == np.less else -np.Inf

    def on_batch_end(self, batch, logs=None):
        current = self.get_monitor_value(logs)
        if current is None:
            return

        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.wait = 0
            if self.restore_best_weights:
                self.best_weights = self.model.get_weights()
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.stopped_batch = batch
                self.model.stop_training = True
                if self.restore_best_weights:
                    if self.verbose > 0:
                        print('Restoring model weights from the end of '
                              'the best batch')
                    self.model.set_weights(self.best_weights)

    def on_train_end(self, logs=None):
        if self.stopped_batch > 0 and self.verbose > 0:
            print('Batch %05d: early stopping' % (self.stopped_batch + 1))

    def get_monitor_value(self, logs):
        monitor_value = logs.get(self.monitor)
        if monitor_value is None:
            warnings.warn(
                'Early stopping conditioned on metric `%s` '
                'which is not available. Available metrics are: %s' %
                (self.monitor, ','.join(list(logs.keys()))), RuntimeWarning
            )
        return monitor_value


def plot_history(history, filename=None):
    '''
    IN:
        - history <return of keras model.fit>
    OUT:
        - None
    INFOS:
        - plots the metrics and losses of a given history object
    '''
    keys = np.array(list(history.history.keys()))
    scores = keys[:int(len(keys)/2)]
    val_scores = keys[int(len(keys)/2):]
    print(scores)
    print(val_scores)
    for score, val_score in zip(scores, val_scores):
        y = history.history[score]
        print(y)
        val_y =  history.history[val_score]
        epochs = range(1,len(y)+1)
        plt.figure(figsize=(11,7))
        plt.plot(epochs, y, 'bo', label='Training')
        plt.plot(epochs, val_y, 'r', label='Test')
        plt.xticks(np.arange(0, 21, step=4))
        plt.xlabel('Epoch', size=18)
        plt.ylabel(score, size=18)
        plt.legend(loc = 'upper right')
        if filename is not None:
            plt.savefig(filename[:-4] + score + ".svg")
        else:
            plt.show()

    return

def test_accuracy(testgen, test_ids, directory_path, model):
    clicks=np.empty((len(test_ids)))
    for i, video_id in enumerate(test_ids):
        f = open(directory_path + "thumbnail_" + video_id + ".csv")
        meta_data_video = json.load(f)
        f.close()

        clicks[i] = int(meta_data_video["clicks"])

    predict = model.predict(testgen)
    predict = np.exp(predict)

    mape = tf.keras.losses.MeanAbsolutePercentageError()
    mape = mape(clicks, predict).numpy()
    print("mape:",mape)
    print("max_error:",np.max(np.fabs(predict - clicks)))
    print("mean_error:", np.mean(np.fabs(predict - clicks)))
    return

def get_test_train_data_generators(video_ids_file_path, test_percent, directory_path, batch_size, image_shape, normalize_clicks, full_net=False, shuffle=True):
    '''
    IN:
        - video_ids_file_path <str>: filepath to file with videoids listed in one line, seperated by spaces
        - test_percent <float>: percentage of data to use for validation
        - directory_path <str>: directory path where the thumbnails and video-meta-data files are stored
        - batch_size <int>: size of batches to load data in
    OUT:
        - traingen, testgen <CustomDataGen>: custom data generators for training and testing data
    INFOS:
        - does the test train split and creates custom data generators for training and testing data to load files in batches while training
    '''
    print("generate data generators")
    with open(video_ids_file_path, "r") as f:
        video_ids = np.array(f.read().splitlines())
    seed(seed_nbr)
    np.random.shuffle(video_ids)
    split_index = int(len(video_ids)*test_percent)
    train_video_ids = video_ids[split_index:]
    test_video_ids = video_ids[:split_index]


    clicks=np.empty((len(train_video_ids)))
    for i, video_id in enumerate(train_video_ids):
        f = open(directory_path + "thumbnail_" + video_id + ".csv")
        meta_data_video = json.load(f)
        f.close()

        clicks[i] = int(meta_data_video["clicks"])
        if clicks[i] < 1:
            clicks[i] = 1
    clicks = np.log(clicks)
    mean = np.mean(clicks)
    std= np.std(clicks)
    mape = tf.keras.losses.MeanAbsolutePercentageError()
    mape_only_mean = mape(clicks, np.full(clicks.shape, mean)).numpy()
    for i in range(300):
        max = np.max(clicks)
        print("max:",max)
        clicks = np.delete(clicks, np.argwhere(clicks == max))
    diff = np.fabs(np.full(clicks.shape, mean)-clicks)
    print(np.mean(diff))
    print(np.max(diff))
    print(f"mape for predicting only the mean: {mape_only_mean}")
    print(f"mean of train_data: {mean}, std of train_data: {std} -> std/mean: {std/mean}")

    if normalize_clicks==0:
        std=1
        mean=0
    elif normalize_clicks==2:
        std=mean
        mean=0


    traingen = CustomDataGen(train_video_ids, directory_path, batch_size=batch_size, input_size=image_shape, mean=mean, std=std, full_net=full_net, shuffle=shuffle)
    testgen = CustomDataGen(test_video_ids, directory_path, batch_size=batch_size, input_size=image_shape, mean=mean, std=std, full_net=full_net, shuffle=shuffle)
    return traingen, testgen, train_video_ids, test_video_ids

def test_only_thubnail_CNN_routine_simple(video_ids_file_path, test_percent=0.2, learning_rate=0.001, directory_path="datafiles/PewDiePie/", epochs=50, batch_size=4, steps_per_epoch=1, validation_steps=1, image_shape=(480, 640, 3), normalize_clicks=0, loss="mean_squared_error", metrics=["mean_absolute_percentage_error", "mean_squared_error"]):
    weights_file_name = "only_thumbnail_CNN_simple_weights_nonorm.h5"

    test_video_ids = []
    with(open(video_ids_file_path, "r")) as f:
        for lines in f:
            test_video_ids.append(lines[:-1])

    batch_size = len(test_video_ids)

    testgen = CustomDataGen(test_video_ids, directory_path, batch_size=batch_size, input_size=image_shape, mean=0, std=1, full_net=False, shuffle=False)

    # setup neural network
    print("configure model")
    img_input = layers.Input(shape=image_shape)
    x = layers.Conv2D(16, 3, activation='relu')(img_input)
    x = layers.MaxPooling2D(2)(x)
    x = layers.Conv2D(32, 3, activation='relu')(x)
    x = layers.MaxPooling2D(2)(x)
    x = layers.Flatten()(x)
    x = layers.Dense(64, activation='relu')(x)
    output = layers.Dense(1, activation=None)(x)
    model = Model(img_input, output)
    #x = layers.Dropout(0.25)(x)

    print(model.summary())
    model.compile(loss=loss,
                  metrics=metrics,
                  optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate))

    model.load_weights("only_thumbnail_CNN_simple_weights_nonorm.h5")
    predictions = model.predict(testgen)
    print(np.exp(np.array(predictions)))
    return

def only_thubnail_CNN_routine_simple(video_ids_file_path, test_percent=0.2, learning_rate=0.001, directory_path="datafiles/PewDiePie/", epochs=50, batch_size=50, steps_per_epoch=720, validation_steps=180, image_shape=(480, 640, 3), normalize_clicks=0, loss="mean_squared_error", metrics=["mean_absolute_percentage_error", "mean_squared_error"]):
    weights_file_name = "only_thumbnail_CNN_simple_weights_nonorm.h5"
    checkpoint = ModelCheckpoint(weights_file_name, monitor='loss', verbose=1, save_best_only=True, mode='auto', save_weights_only=True, save_freq="epoch")
    #early_stopper = BatchEarlyStopping(monitor='loss', patence=50, restore_best_weights=True)
    early_stopper = EarlyStopping(monitor='loss', patience=5, restore_best_weights=True)

    # get train and test data generators
    traingen, testgen, train_ids, test_ids = get_test_train_data_generators(video_ids_file_path, test_percent, directory_path, batch_size, image_shape, normalize_clicks, shuffle=True)

    # setup neural network
    print("configure model")
    img_input = layers.Input(shape=image_shape)
    x = layers.Conv2D(16, 3, activation='relu')(img_input)
    x = layers.MaxPooling2D(2)(x)
    x = layers.Conv2D(32, 3, activation='relu')(x)
    x = layers.MaxPooling2D(2)(x)
    x = layers.Flatten()(x)
    x = layers.Dense(64, activation='relu')(x)
    output = layers.Dense(1, activation=None)(x)
    model = Model(img_input, output)
    #x = layers.Dropout(0.25)(x)

    print(model.summary())
    model.compile(loss=loss,
                  metrics=metrics,
                  optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate))

    model.load_weights("only_thumbnail_CNN_simple_weights_nonorm.h5")
    # train neural network
    print("train model")
    history = model.fit(traingen,
              validation_data=testgen,
              steps_per_epoch=steps_per_epoch,
              batch_size=batch_size,
              epochs=epochs,
              validation_steps=validation_steps,
              callbacks=[checkpoint, early_stopper],
              verbose=1)

    plot_history(history, filename="./datafiles/plots/broad_progress_simplenet.svg")

    model.save_weights(weights_file_name[:-3] + "_finished.h5")

    #test_accuracy(testgen, test_ids, directory_path, model)

    #cm1 = metrics.confusion_matrix(y_test, y_pred)
    return

def only_thubnail_CNN_routine_resnet(video_ids_file_path, test_percent=0.2, learning_rate=0.01, directory_path="datafiles/PewDiePie/", epochs=50, batch_size=50, steps_per_epoch=720, validation_steps=180, image_shape=(480, 640, 3), normalize_clicks=0, loss="mean_squared_error", metrics=["mean_absolute_percentage_error", "mean_squared_error"]):
    weights_file_name = "only_thumbnail_CNN_resnet_weights_nonorm.h5"
    checkpoint = ModelCheckpoint(weights_file_name, monitor='loss', verbose=1, save_best_only=True, mode='auto', save_weights_only=True, save_freq="epoch")
    #early_stopper = BatchEarlyStopping(monitor='loss', patience=50, restore_best_weights=True)
    early_stopper = EarlyStopping(monitor='loss', patience=5, restore_best_weights=True)

    # get train and test data generators
    traingen, testgen, train_ids, test_ids = get_test_train_data_generators(video_ids_file_path, test_percent, directory_path, batch_size, image_shape, normalize_clicks, shuffle=True)

    # setup neural network
    print("configure model")
    img_input = layers.Input(shape=image_shape)
    base_model = tf.keras.applications.resnet50.ResNet50(
        include_top=False,
        weights='imagenet',
        input_tensor=img_input,
        input_shape=image_shape,
        pooling=max,
    )
    base_model.trainable = False
    x = base_model(img_input, training=False)
    x = layers.Flatten()(x)
    x = layers.Dropout(0.25)(x)
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dropout(0.25)(x)
    output = layers.Dense(1, activation=None)(x)
    model = Model(img_input, output)
    #x = layers.Dropout(0.25)(x)

    print(model.summary())
    model.compile(loss=loss,
                  metrics=metrics,
                  optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate))

    model.load_weights('only_thumbnail_CNN_resnet_weights_nonorm.h5')
    # train neural network
    print("train model")
    history = model.fit(traingen,
              validation_data=testgen,
              steps_per_epoch=steps_per_epoch,
              batch_size=batch_size,
              epochs=epochs,
              validation_steps=validation_steps,
              callbacks=[checkpoint, early_stopper],
              verbose=1)

    plot_history(history, filename="./datafiles/plots/broad_progress_resnet.svg")

    #finetune model
    print("finetune model")
    base_model.trainable = True
    model.compile(loss=loss,
                  metrics=metrics,
                  optimizer=tf.keras.optimizers.Adam(1e-5))
    history = model.fit(traingen,
              validation_data=testgen,
              steps_per_epoch=steps_per_epoch,
              batch_size=batch_size,
              epochs=epochs,
              validation_steps=validation_steps,
              callbacks=[checkpoint, early_stopper],
              verbose=1)
    plot_history(history, filename="./datafiles/plots/fine_progress_resnet.svg")

    model.save_weights(weights_file_name[:-3] + "_finished.h5")

    #test_accuracy(testgen, test_ids, directory_path, model)

    #cm1 = metrics.confusion_matrix(y_test, y_pred)
    return

def tumbnail_and_metadata_CNN_resnet(video_ids_file_path, test_percent=0.2, learning_rate=0.001, directory_path="datafiles/MrBeast/", epochs=50, batch_size=50, steps_per_epoch=720, validation_steps=180, image_shape=(480, 640, 3), normalize_clicks=0, loss="mean_squared_error", metrics=["mean_absolute_percentage_error", "mean_squared_error"]):
    weights_file_name = "full_CNN_resnet_weights_nonorm.h5"
    checkpoint = ModelCheckpoint(weights_file_name, monitor='loss', verbose=1, save_best_only=True, mode='auto', save_weights_only=True, save_freq="epoch")
    #early_stopper = BatchEarlyStopping(monitor='loss', patience=50, restore_best_weights=True)
    early_stopper = EarlyStopping(monitor='loss', patience=5, restore_best_weights=True)
    # get train and test data generators
    traingen, testgen, train_ids, test_ids = get_test_train_data_generators(video_ids_file_path, test_percent, directory_path, batch_size, image_shape, normalize_clicks, full_net=True, shuffle=True)

    # setup neural network
    print("configure model")
    # image model part
    img_input = layers.Input(shape=image_shape)
    base_model = tf.keras.applications.resnet50.ResNet50(
        include_top=False,
        weights='imagenet',
        input_tensor=img_input,
        input_shape=image_shape,
        pooling=max,
    )
    base_model.trainable = False
    x = base_model(img_input, training=False)
    x = layers.Flatten()(x)
    x = layers.Dropout(0.25)(x)
    x = layers.Dense(16, activation='relu')(x)

    # meta data model part
    meta_input = layers.Input(shape=(meta_data_space))
    y = layers.Dense(128, activation='relu')(meta_input)
    y = layers.Dropout(0.25)(y)
    y = layers.Dense(32, activation='relu')(y)
    y = layers.Dropout(0.25)(y)
    y = layers.Dense(16, activation='relu')(y)

    # combined model
    x_y = layers.concatenate([x,y],name="concatenated_layer")
    x_y = layers.Dense(16, activation='relu')(x_y)
    x_y = layers.Dropout(0.25)(x_y)
    x_y = layers.Dense(16, activation='relu')(x_y)
    output = layers.Dense(1, activation=None)(x_y)
    model = Model(inputs=[(img_input, meta_input)], outputs=[output])
    #x = layers.Dropout(0.25)(x)

    ## -> compressor in the beginning, which kind of attributes to use?
    ## -> normierung nochmal probieren (der clicks)
    ##nohub

    print(model.summary())
    model.compile(loss=loss,
                  metrics=metrics,
                  optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate))

    # train neural network
    print("train model")
    history = model.fit(traingen,
              validation_data=testgen,
              steps_per_epoch=steps_per_epoch,
              batch_size=batch_size,
              epochs=epochs,
              validation_steps=validation_steps,
              callbacks=[checkpoint, early_stopper],
              verbose=1)

    plot_history(history, filename="./datafiles/plots/broad_progress_full_resnet_nonorm.svg")

    model.save_weights(weights_file_name[:-3] + "_finished.h5")

    #test_accuracy(testgen, test_ids, directory_path, model)

    return





number_of_hues = 176
title_word_space = 103
number_of_title_words = 20
title_words_space = number_of_title_words * title_word_space
number_of_thumbnail_words = 10
thumbnail_word_space = title_word_space + 3
thumbnail_words_space = thumbnail_word_space*number_of_thumbnail_words
number_of_faces = 10
space_per_face = 10
emotion_space = space_per_face*number_of_faces
meta_data_space = number_of_hues + title_words_space + thumbnail_words_space + emotion_space + 1 + 1 + 1

max_video_length = 1*60*60

video_ids_file_path = "test_data/thumbnail_IDS.txt"

#only_thubnail_CNN_routine_simple(video_ids_file_path, directory_path="test_data/", normalize_clicks=0)
test_only_thubnail_CNN_routine_simple(video_ids_file_path, directory_path="test_data/", normalize_clicks=0)
#tumbnail_and_metadata_CNN_resnet(video_ids_file_path, directory_path="datafiles/MrBeast/", normalize_clicks=0)
#only_thubnail_CNN_routine_resnet(video_ids_file_path, directory_path="datafiles/MrBeast/", normalize_clicks=0)

#tumbnail_and_metadata_CNN_resnet(video_ids_file_path, directory_path="datafiles/MrBeast/",)

# cut off video length at some number? -> normalize that?
# also normalize the release date? by the most recent one? some date in 2022?
# check the loss if big outliers are taken out at the end
# normalize better?
# -> classify data into batches??
# batch normalization
# wie viele werte weichen um 50% ab oder so??
# adaptable learning rate

'''
picture = keras.utils.load_img("../../0.jpeg")
picture = tf.image.resize(picture, [480, 640])
picture = keras.utils.img_to_array(picture).T
picture = np.array([np.array(i).T for i in picture])

model = keras.models.load_model("./datafiles/ipa_model")
print("test 1")
model.compile()
print("test 2")
#print(model.summary())
print(model.predict(np.array([picture,picture])))
'''
